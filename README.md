# `transqlate`: Transpile any SQL to PostgreSQL Dialect

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/dalibo/transqlate.svg)](https://pkg.go.dev/gitlab.com/dalibo/transqlate)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/dalibo/transqlate)](https://goreportcard.com/report/gitlab.com/dalibo/transqlate)

:warning: **Project is early in its development** :warning:


## Features

- translate Oraclisms and MySQLisms to PostgreSQL dialect
- handle identifier case
- preserve whitespaces, case and comments
- top-notch error reporting
- parses script or any expression
- simple CLI with syntax highlighting or Go idiomatic API
- prettify following [Simon Holywell guide](https://www.sqlstyle.guide/)


### CLI Example

Use `transqlate` tool to translate any Oracle SQL snippet to PostgreSQL dialect.

``` console
$ transqlate
Type SQL snippet and terminate with ^D
SELECT SYSDATE FROM DUAL; -- my comment
^D
SELECT CURRENT_TIMESTAMP; -- my comment
$
```


### Go Example

This simplest entrypoint is the `oracle` package.

``` go
package main

import (
	"fmt"

	"gitlab.com/dalibo/transqlate/oracle"
)

func main() {
	pgsql, err := oracle.Translate("file.sql", `SELECT SYSDATE FROM DUAL;`)
	if err != nil {
		panic(err.Error())
	}
	fmt.Println(pgsql)
	// Output: SELECT LOCALTIMESTAMP;
}
```

## Installation

Use packages or binary available on [Releases page](https://gitlab.com/dalibo/transqlate/-/releases).


## Roadmap

- more SQL syntax
- more rewriting rules
- more source dialects : mssql, sybase, etc.

Feel free to share your cases by opening a [GitLab issue] !

[GitLab issue]: https://gitlab.com/bersace/transqlate/-/issues/new

The main target dialect is PostgreSQL.
Feel free to reuse transqlate to target another dialect.


## Support

If you found a bug or have a feature request, please file a [GitLab issue].


## Authors

Transqlate is a [Dalibo Labs] project available under PostgreSQL license.

[Dalibo Labs]: https://labs.dalibo.com/


## References

- [Ora2Pg](https://ora2pg.darold.net/) - the venerable open source reference for migration to PostgreSQL.
- [sqlparser-rs](https://github.com/sqlparser-rs/sqlparser-rs) - a TDOP SQL parser for Rust
- [sqlglot](https://github.com/tobymao/sqlglot/) - a SQL frontend for Python app
- [pg_query_go](https://github.com/pganalyze/pg_query_go) - use Postgres parser in Go
