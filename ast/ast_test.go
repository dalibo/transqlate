package ast_test

import (
	"fmt"
	"log/slog"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestMain(m *testing.M) {
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	})))
	os.Exit(m.Run())
}

func TestVisit(t *testing.T) {
	r := require.New(t)
	var n ast.Node = ast.Statements{
		// Don't bother with tokens
		Statements: []ast.Node{
			ast.Statement{Expression: ast.Select{
				List: ast.Items{{Expression: ast.Leaf{}}},
				Where: ast.Where{
					Keyword: lexer.MustLex("WHERE")[0],
					Conditions: []ast.Prefix{
						{Expression: ast.Infix{
							Left:  ast.Postfix{Expression: ast.Leaf{}},
							Right: ast.Leaf{},
						}},
					},
				},
			}},
			ast.Call{
				Function: ast.Leaf{},
				Args:     ast.Items{{Expression: ast.Leaf{}}},
			},
		},
	}

	// If you add a node in n, update here the name of the type, without package name.
	expectedVisitOrder := []string{
		"Leaf",    // Select.List[0].Expression
		"Postfix", // Select.List[0]
		"Leaf",    // Postfix.Expression
		"Postfix",
		"Leaf", // Infix.Right
		"Infix",
		"Prefix", // Where.Conditions[0]
		"Where",  // Select.Where
		"Select",
		"Statement",
		"Leaf",    // Call.Function
		"Leaf",    // Args.Items[0].Expression
		"Postfix", // Args.Items[0]
		"Call",
		"Statements",
	}

	// Visit all node to test full walk.
	// List type name of node in visit order
	var visited []string
	n.Visit(func(n ast.Node) ast.Node {
		visited = append(visited, strings.ReplaceAll(fmt.Sprintf("%T", n), "ast.", ""))
		return n
	})
	r.Equal(expectedVisitOrder, visited)
}

func TestTransformStart(t *testing.T) {
	r := require.New(t)

	editor := func(t lexer.Token) lexer.Token {
		t.Type = lexer.Identifier
		return t.Set(`EDITED`)
	}

	n := parser.MustParse("1/2+3").TransformStart(editor)
	r.Equal("EDITED/2+3", lexer.Write(n))

	n = parser.MustParse("(1,2)").TransformStart(editor)
	r.Equal("EDITED1,2)", lexer.Write(n))

	n = parser.MustParse("SELECT *").TransformStart(editor)
	r.Equal("EDITED *", lexer.Write(n))
}

func TestTransformEnd(t *testing.T) {
	r := require.New(t)

	editor := func(t lexer.Token) lexer.Token {
		t.Type = lexer.Identifier
		return t.Set(`EDITED`)
	}

	n := parser.MustParse("'leaf'").TransformEnd(editor)
	r.Equal(`EDITED`, lexer.Write(n))

	n = parser.MustParse("1+2+3").TransformEnd(editor)
	r.Equal("1+2+EDITED", lexer.Write(n))

	n = parser.MustParse("(1 )").TransformEnd(editor)
	r.Equal("(1 EDITED", lexer.Write(n))

	n = parser.MustParse("1 ;").TransformEnd(editor)
	r.Equal("1 EDITED", lexer.Write(n))

	n = parser.MustParse("SELECT 1 ;").TransformEnd(editor)
	r.Equal("SELECT 1 EDITED", lexer.Write(n))

	n = parser.MustParse("CASE 1 ELSE 2 END").TransformEnd(editor)
	r.Equal("CASE 1 ELSE 2 EDITED", lexer.Write(n))
}
