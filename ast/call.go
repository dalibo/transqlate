package ast

import "gitlab.com/dalibo/transqlate/lexer"

// Call holds a function call.
type Call struct {
	Function Node // Identifier or qualified name
	Open     lexer.Token
	Distinct lexer.Token
	Args     Items
	Clauses  []Node
	Close    lexer.Token
}

func (c Call) Tokens() []lexer.Token {
	t := c.Function.Tokens()
	t = append(t, c.Open, c.Distinct)
	t = append(t, c.Args.Tokens()...)
	for _, clause := range c.Clauses {
		t = append(t, clause.Tokens()...)
	}
	return append(t, c.Close)
}

func (c Call) TransformStart(f Transformer[lexer.Token]) Node {
	c.Function = c.Function.TransformStart(f)
	return c
}

func (c Call) TransformEnd(f Transformer[lexer.Token]) Node {
	c.Close = f(c.Close)
	return c
}

func (c Call) Visit(f Transformer[Node]) Node {
	c.Function = c.Function.Visit(f)
	originalArgs := append([]Postfix{}, c.Args...)
	c.Args = nil
	for _, arg := range originalArgs {
		n := arg.Visit(f)
		if n == nil {
			continue
		}
		c.Args = append(c.Args, n.(Postfix))
	}

	originalClauses := append([]Node{}, c.Clauses...)
	c.Clauses = nil
	for _, clause := range originalClauses {
		n := clause.Visit(f)
		if n == nil {
			continue
		}
		c.Clauses = append(c.Clauses, n)
	}
	return f(c)
}

// Is reports whether the function name matches the given strings.
//
// Accepts one or two strings. If two strings are given, the first one is the
// package/schema name and the second one is the function name.
func (c Call) Is(s ...string) bool {
	if len(s) == 1 {
		leaf, _ := c.Function.(Leaf)
		return leaf.In(s[0])
	}

	if len(s) == 2 {
		infix, _ := c.Function.(Infix)
		if !infix.Is(".") {
			return false
		}
		left, right := lexer.Edges(infix)
		return left.Str == s[0] && right.Str == s[1]
	}

	panic("accepts one or two arguments")
}

// In reports whether function name is in the given list of strings.
func (c Call) In(s ...string) bool {
	for _, v := range s {
		if c.Is(v) {
			return true
		}
	}
	return false
}

func (c Call) indent(indentation string) Node {
	c.Function = Indent(c.Function, indentation)
	_, before := lexer.Edges(c.Function)
	// Move indentation after function name.
	indentation = lexer.IncreaseIndentation(indentation, lexer.Write(c.Function))
	c.Args, _ = IndentAfter(before, c.Args, indentation)
	// Remove space between function and args.
	c.Args = c.Args.TransformStart(func(t lexer.Token) lexer.Token {
		if t.LeadingComment() != "" {
			return t
		}
		t.Prefix = ""
		return t
	}).(Items)
	return c
}

type Aggregate struct {
	Call        Call
	WithinGroup Node
	Over        Node
}

func (agg Aggregate) Tokens() []lexer.Token {
	t := agg.Call.Tokens()
	if agg.WithinGroup != nil {
		t = append(t, agg.WithinGroup.Tokens()...)
	}
	if agg.Over != nil {
		t = append(t, agg.Over.Tokens()...)
	}
	return t
}

func (agg Aggregate) TransformStart(f Transformer[lexer.Token]) Node {
	agg.Call = agg.Call.TransformStart(f).(Call)
	return agg
}

func (agg Aggregate) TransformEnd(f Transformer[lexer.Token]) Node {
	if agg.Over != nil {
		agg.Over = agg.Over.TransformEnd(f)
		return agg
	}
	if agg.WithinGroup != nil {
		agg.WithinGroup = agg.WithinGroup.TransformEnd(f)
		return agg
	}
	agg.Call = agg.Call.TransformEnd(f).(Call)
	return agg
}

func (agg Aggregate) Visit(f Transformer[Node]) Node {
	agg.Call = agg.Call.Visit(f).(Call)
	if agg.WithinGroup != nil {
		agg.WithinGroup = agg.WithinGroup.Visit(f)
	}
	if agg.Over != nil {
		agg.Over = agg.Over.Visit(f)
	}
	// Vanish aggregate without within or over clauses.
	if agg.WithinGroup == nil && agg.Over == nil {
		return agg.Call
	}
	return f(agg)
}
