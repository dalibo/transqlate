package ast_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
)

func TestCallIs(t *testing.T) {
	r := require.New(t)

	// Function node is a Leaf.
	c := parser.MustParse("foo()").(ast.Call)
	r.True(c.Is("foo"))
	r.False(c.Is("foo", "baz"))

	// Function node is an Infix.
	c = parser.MustParse("foo.bar()").(ast.Call)
	r.True(c.Is("foo", "bar"))
	r.False(c.Is("foo"))
	r.False(c.Is("bar"))

	// Function is an unhandled type.
	// Here, an unexpected Call.
	c = parser.MustParse("foo()()").(ast.Call)
	r.False(c.Is("foo"))
}
