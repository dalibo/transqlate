package ast

import (
	"gitlab.com/dalibo/transqlate/lexer"
)

// Expression is a generic expression.
//
// Use it for unspecified expression.
type Expression []lexer.Token

func (e Expression) Tokens() []lexer.Token {
	return e
}

func (e Expression) TransformStart(f Transformer[lexer.Token]) Node {
	e[0] = f(e[0])
	return e
}

func (e Expression) TransformEnd(f Transformer[lexer.Token]) Node {
	e[len(e)-1] = f(e[len(e)-1])
	return e
}

func (e Expression) Visit(f Transformer[Node]) Node {
	return f(e)
}

// Is checks expression tokens
//
// Accepts a list of normalized token as string.
// Empty string is a wildcard.
// Token type is not checked.
//
// Ex: e.Is("ON", "OVERFLOW", "")
func (e Expression) Is(tokens ...string) bool {
	if len(tokens) > len(e) {
		return false
	}
	for i, token := range tokens {
		if token == "" {
			continue
		}
		if e[i].Str != token {
			return false
		}
	}
	return true
}

// Alias renames an expression with or without AS keyword.
type Alias struct {
	Expression Node
	// As may be Void if AS keyword is omitted.
	As lexer.Token
	// Name is the new name, an identifier.
	Name lexer.Token
}

func (a Alias) String() string {
	return a.Name.Str
}

func (a Alias) Tokens() []lexer.Token {
	t := a.Expression.Tokens()
	t = append(t, a.As)
	return append(t, a.Name)
}

func (a Alias) TransformStart(f Transformer[lexer.Token]) Node {
	a.Expression = a.Expression.TransformStart(f)
	return a
}

func (a Alias) TransformEnd(f Transformer[lexer.Token]) Node {
	a.Name = f(a.Name)
	return a
}

func (a Alias) Visit(f Transformer[Node]) Node {
	a.Expression = a.Expression.Visit(f)
	if a.Expression == nil {
		return nil
	}
	return f(a)
}

func (a Alias) indent(indentation string) Node {
	a.Expression = Indent(a.Expression, indentation)
	return a
}

// AliasTo ensure expr has a name.
//
// If expr is an alias, it renames it.
// If expr is not an alias, it creates a new alias with the given name.
func AliasTo(expr Node, name string) Alias {
	alias, ok := expr.(Alias)
	if ok {
		alias.Name.Set(name)
		return alias
	}
	alias.Expression = expr
	tokens := lexer.MustLex(` AS "%s"`, name)
	alias.As = tokens[0]
	alias.Name = tokens[1]
	alias, alias.Expression = SwapWhitespaces(alias, expr)
	return alias
}

// Between holds BETWEEN or NOT BETWEEN expression.
type Between struct {
	Expression Node
	Between    []lexer.Token
	Start      Node
	And        lexer.Token
	End        Node
}

func (b Between) Tokens() []lexer.Token {
	t := b.Expression.Tokens()
	t = append(t, b.Between...)
	t = append(t, b.Start.Tokens()...)
	t = append(t, b.And)
	t = append(t, b.End.Tokens()...)
	return t
}

func (b Between) TransformStart(f Transformer[lexer.Token]) Node {
	b.Expression = b.Expression.TransformStart(f)
	return b
}

func (b Between) TransformEnd(f Transformer[lexer.Token]) Node {
	b.End = b.End.TransformEnd(f)
	return b
}

func (b Between) Visit(f Transformer[Node]) Node {
	b.Expression = b.Expression.Visit(f)
	if b.Expression == nil {
		return nil
	}
	b.Start = b.Start.Visit(f)
	if b.Start == nil {
		return nil
	}
	b.End = b.End.Visit(f)
	if b.End == nil {
		return nil
	}
	return f(b)
}
