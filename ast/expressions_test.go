package ast_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestAlias(t *testing.T) {
	r := require.New(t)
	a := ast.Alias{
		Expression: ast.Leaf{Token: lexer.MustLex("foo")[0]},
		As:         lexer.MustLex(" AS")[0],
		Name:       lexer.MustLex(" a")[0],
	}
	r.Equal("foo AS a", lexer.Write(a))

	a = ast.Alias{
		Expression: ast.Leaf{Token: lexer.MustLex("foo")[0]},
		Name:       lexer.MustLex(" a")[0],
	}
	r.Equal("foo a", lexer.Write(a))
}
