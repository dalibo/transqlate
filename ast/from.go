package ast

import (
	"slices"
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
)

// TableName guess table expression name in FROM clause.
//
// Accepts <table>, schema.<table> and expr AS <table>, ... JOIN <table>.
func TableName(expr Node) string {
	switch expr := expr.(type) {
	case Join:
		return TableName(expr.Right)
	case Alias: // X.Y AS mytable, (SELECT ...) mytable, ...
		return expr.Name.Str // Return mytable.
	case Leaf: // plain SELECT FROM mytable
		return expr.Token.Str // return mytable
	case Infix: // SELECT FROM namespace.mytable.
		if expr.Is(".") {
			return TableName(expr.Right) // Return mytable
		}
	}
	return "" // Anonymous table
}

// From holds the FROM clause of a SELECT query.
type From struct {
	From   lexer.Token
	Tables Items
}

func (from From) IsZero() bool {
	return from.From.IsZero() && len(from.Tables) == 0
}

func (from From) Tokens() []lexer.Token {
	t := []lexer.Token{from.From}
	t = append(t, from.Tables.Tokens()...)
	return t
}

func (from From) TransformStart(f Transformer[lexer.Token]) Node {
	from.From = f(from.From)
	return from
}

func (from From) TransformEnd(f Transformer[lexer.Token]) Node {
	i := len(from.Tables) - 1
	from.Tables[i] = from.Tables[i].TransformEnd(f).(Postfix)
	return from
}

func (from From) Visit(f Transformer[Node]) Node {
	from.Tables = from.Tables.Visit(f).(Items)
	return f(from)
}

func (from From) indent(indentation string) Node {
	from.From = from.From.LeftIndent(indentation)
	from.Tables, _ = IndentAfter(from.From, from.Tables, indentation)
	return from
}

// Flatten the FROM clause.
//
// Represent all table expressions as an array.
// Joins loose their left hand side.
// Spurious parenthesis are removed.
func (from From) Flatten() Items {
	var tableList Items
	for i := range from.Tables {
		tableList = append(tableList, flattenTableExpression(from.Tables[i])...)
	}
	return tableList
}

func flattenTableExpression(item Postfix) Items {
	var tableList Items
	switch expr := item.Expression.(type) {
	case Join:
		tableList = append(tableList, flattenTableExpression(Postfix{Expression: expr.Left})...)
		expr.Left = nil
		item.Expression = expr
		tableList = append(tableList, item)
	case Array:
		if len(expr.Items) == 0 {
			return nil
		}
		_, isSubselect := expr.Items[0].Expression.(Select)
		if len(expr.Items) == 1 && isSubselect {
			// Keep subselects as is.
			tableList = append(tableList, item)
		} else {
			// Unwrap array of table expressions.
			for _, item := range expr.Items {
				tableList = append(tableList, flattenTableExpression(item)...)
			}
		}
	default:
		tableList = append(tableList, item)
	}
	return tableList
}

// Append a table expression to FROM clause.
//
// Ensures FROM keyword is set.
// Indents JOIN with FROM and tables with previous table.
// Ensures JOIN is on its own line.
func (from *From) Append(table Node) {
	if from.IsZero() {
		from.From = lexer.MustLex(" FROM")[0]
	}

	_, isJoin := table.(Join)
	if isJoin {
		last := len(from.Tables) - 1
		// Ensure previous table ends line to have JOIN on its own line.
		from.Tables[last] = EndLine(from.Tables[last])
		// Indent JOIN with FROM.
		item := IndentFrom(Postfix{Expression: table}, from)
		// Skip comma before first table.
		from.Tables = append(from.Tables, item)
	} else {
		// Append like a regular comma list item.
		from.Tables.Append(table)

		l := len(from.Tables)
		if l < 2 {
			return
		}

		previous := from.Tables[l-2]
		_, isJoin := previous.Expression.(Join)
		if !isJoin {
			return
		}

		// Indent tables with previous table.
		from.Tables[l-2] = EndLine(previous)
		from.Tables[l-1] = IndentFrom(from.Tables[l-1], previous)
	}
}

// IsJoin reports whether a table expression is a JOIN.
//
// table is the name of the table in the FROM clause.
func (from From) IsJoin(table string) bool {
	index := from.Index(table)
	if index == -1 {
		return false
	}
	_, isJoin := from.Tables[index].Expression.(Join)
	return isJoin
}

// Replace a table expression by name.
func (from *From) Replace(table string, expr Node) {
	index := from.Index(table)
	if index == -1 {
		panic("unknown table")
	}
	from.Tables[index].Expression = expr

	_, isJoin := expr.(Join)
	if isJoin { // Void comma before join.
		if index == 0 {
			panic("putting JOIN first in FROM clause")
		}
		previous := from.Tables[index-1]
		if !previous.Token.IsZero() {
			// Replacing a table with a JOIN. Remove previous comma.
			previous.Expression = SpaceFrom(previous.Expression, previous)
			previous.Token = lexer.Token{}
			from.Tables[index-1] = previous
		}
	} else if index > 0 {
		previous := from.Tables[index-1]
		if previous.Token.IsZero() {
			// Replacing a JOIN with a table. Ensure comma before table.
			previous.Token = lexer.MustLex(",")[0]
			previous = SpaceFrom(previous, previous.Expression)
		}
		_, isJoin := previous.Expression.(Join)
		if isJoin {
			// Ensure previous JOIN is on its own line.
			previous = EndLine(previous)
		}
		from.Tables[index-1] = previous
	}
}

// Names of all table expression in FROM clause.
//
// Returns anonymous expression as empty string.
func (from From) Names() (names []string) {
	for _, item := range from.Tables {
		names = append(names, TableName(item.Expression))
	}
	return
}

// Contains checks existence of table expression in FROM.
func (from From) Contains(table string) bool {
	return from.Index(table) != -1
}

// Index return position of table in FROM or -1.
func (from From) Index(table string) int {
	for i, item := range from.Tables {
		if TableName(item.Expression) == table {
			return i
		}
	}
	return -1
}

// Sort table names in the order they appear in FROM clause.
//
// Panics if a name is not in FROM.
func (from From) Sort(names ...string) (sorted []string) {
	for _, item := range from.Tables {
		if slices.Contains(names, TableName(item.Expression)) {
			sorted = append(sorted, TableName(item.Expression))
		}
	}

	if len(sorted) != len(names) {
		panic("missing table in FROM clause")
	}

	return
}

// Join holds an explicit JOIN clause.
//
// This node can be considered either as an operator or an expression. The
// parser instantiates JOIN as an operator between two table expressions. To
// ease rewriting, the From.Flatten split JOIN as two expression: the left and
// the JOIN. This is more natural for human to consider JOINs as a list of
// expression rather than a tree of operation.
type Join struct {
	Left      Node // Left table, not join direction. May be nil for JOIN expression.
	Join      []lexer.Token
	Right     Node // Right table, not join direction.
	Condition Node // nil or an [ast.Where] clause with ON or USING keyword.
}

func (j Join) Tokens() (tokens []lexer.Token) {
	if j.Left != nil {
		tokens = append(tokens, j.Left.Tokens()...)
	}
	tokens = append(tokens, j.Join...)
	if j.Right != nil {
		tokens = append(tokens, j.Right.Tokens()...)
	}
	if j.Condition != nil {
		tokens = append(tokens, j.Condition.Tokens()...)
	}
	return
}

func (j Join) TransformStart(f Transformer[lexer.Token]) Node {
	if j.Left != nil {
		j.Left = j.Left.TransformStart(f)
	} else {
		j.Join[0] = f(j.Join[0])
	}
	return j
}

func (j Join) TransformEnd(f Transformer[lexer.Token]) Node {
	if j.Condition == nil {
		j.Right = j.Right.TransformEnd(f)
	} else {
		j.Condition = j.Condition.TransformEnd(f)
	}
	return j
}

func (j Join) Visit(f Transformer[Node]) Node {
	if j.Left != nil {
		j.Left = j.Left.Visit(f)
	}
	j.Right = j.Right.Visit(f)
	if j.Condition != nil {
		j.Condition = j.Condition.Visit(f)
	}
	return f(j)
}

func (j Join) indent(indentation string) Node {
	// Don't touch left hand side.
	if len(j.Join) == 1 {
		j.Join[0] = j.Join[0].LeftIndent(indentation)
	} else {
		j.Join[0] = j.Join[0].Indent(indentation)
	}
	before := j.Join[len(j.Join)-1]
	j.Right, before = IndentAfter(before, j.Right, indentation)
	if j.Condition != nil {
		if len(j.Join) > 1 {
			keyword, _ := lexer.Edges(j.Condition) // ON or USING
			// Push * JOIN condition after the river.
			indentation += strings.Repeat(" ", len(keyword.Raw)+1)
		}
		j.Condition, _ = IndentAfter(before, j.Condition, indentation)
	}
	return j
}
