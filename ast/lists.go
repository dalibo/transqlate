package ast

import (
	"fmt"
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
)

type Items []Postfix

func (items Items) Tokens() (tokens []lexer.Token) {
	for _, item := range items {
		tokens = append(tokens, item.Tokens()...)
	}
	return
}

func (items Items) Visit(f Transformer[Node]) Node {
	newItems := Items{}
	for _, item := range items {
		n := item.Visit(f)
		if n == nil {
			continue
		}
		newItems = append(newItems, n.(Postfix))
	}
	return newItems
}

func (items Items) TransformStart(f Transformer[lexer.Token]) Node {
	items[0] = items[0].TransformStart(f).(Postfix)
	return items
}

func (items Items) TransformEnd(f Transformer[lexer.Token]) Node {
	i := len(items) - 1
	items[i] = items[i].TransformEnd(f).(Postfix)
	return items
}

func (items Items) indent(indentation string) Node {
	var before lexer.Token
	for i, item := range items {
		item, before = IndentAfter(before, item, indentation)
		items[i] = item
	}
	return items
}

// Append adds a new item to the list.
//
// Ensure a comma between items.
func (items *Items) Append(n Node) {
	if l := len(*items); l > 0 {
		end := l - 1
		(*items)[end] = (*items)[end].SetOperator(lexer.MustLex(",")[0])
	}
	*items = append(*items, Postfix{Expression: n})
}

// Delete removes an item at position.
func (items *Items) Delete(pos int) (expr Node) {
	l := len(*items)
	if pos < 0 || l <= pos {
		panic(fmt.Sprintf("position %d out of range", pos))
	}
	expr = SpaceFrom((*items)[pos].Expression, (*items)[pos])
	*items = append((*items)[:pos], (*items)[pos+1:]...)
	if pos == l-1 {
		end := l - 2
		// Remove trailing comma.
		(*items)[end] = (*items)[end].RemoveOperator()
	}
	return
}

// Insert adds a new item to the list at the given position.
//
// Panics if position is out of range.
func (items *Items) Insert(pos int, n Node) {
	if pos < 0 || len(*items) < pos {
		panic(fmt.Sprintf("position %d out of range", pos))
	}

	i := Postfix{Expression: n}
	comma := lexer.MustLex(",")[0]

	if pos != len(*items) {
		i = i.SetOperator(comma)
	}

	*items = append((*items)[:pos], append([]Postfix{i}, (*items)[pos:]...)...)
}

// Array holds comma separated expressions, eventually braced by parenthesis.
//
// e.g. a.id, a.name or (1, 2) or [3, 4]
//
// Open and Close can be Void for SELECT list, FROM clause.
// All Items are Postfix node with separator as postfix operator.
// Last Item has void operator because SQL does not allow trailing comma.
//
// Used for function call arguments, sub-queries, function signature, etc.
type Array struct {
	Open  lexer.Token
	Items Items
	Close lexer.Token
}

func (g Array) Tokens() []lexer.Token {
	t := []lexer.Token{}
	t = append(t, g.Open)
	for _, i := range g.Items {
		t = append(t, i.Tokens()...)
	}
	t = append(t, g.Close)
	return t
}

func (g Array) TransformStart(f Transformer[lexer.Token]) Node {
	g.Open = f(g.Open)
	return g
}

func (g Array) TransformEnd(f Transformer[lexer.Token]) Node {
	g.Close = f(g.Close)
	return g
}

func (g Array) Visit(f Transformer[Node]) Node {
	g.Items = g.Items.Visit(f).(Items)
	return f(g)
}

func (g Array) indent(indentation string) Node {
	g.Open = g.Open.Indent(indentation)
	if g.Open.Suffix == "\n" {
		g.Open.Suffix = ""
	}
	before := g.Open

	itemIndentation := indentation + strings.Repeat(" ", len(g.Open.Raw))
	for i, item := range g.Items {
		item, before = IndentAfter(before, item, itemIndentation)
		g.Items[i] = item
	}

	if before.EndsLine() {
		g.Close = g.Close.Indent(indentation)
	}

	return g
}
