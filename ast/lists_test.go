package ast_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestItemsEdit(t *testing.T) {
	r := require.New(t)
	a := parser.MustParse(dedent.Dedent(`
	(
	-- before
	1, -- after
	-- before 2
	2 -- after 2
	)
	`)).(ast.Array)

	a.Items.Append(parser.MustParse(dedent.Dedent(`
	-- before 3
	3 -- after 3
	`)[1:]))

	// Put comma before comment.
	r.Equal(dedent.Dedent(`
	(
	-- before
	1, -- after
	-- before 2
	2, -- after 2
	-- before 3
	3 -- after 3
	)
	`), lexer.Write(a))

	// Put comma before comment.
	a.Items.Insert(0, parser.MustParse(dedent.Dedent(`
	-- before 0
	0 -- after 0
	`)[1:]))
	r.Equal(dedent.Dedent(`
	(
	-- before 0
	0, -- after 0
	-- before
	1, -- after
	-- before 2
	2, -- after 2
	-- before 3
	3 -- after 3
	)
	`), lexer.Write(a))

	// Remove trailing comma before comment.
	a.Items.Delete(2)
	r.Equal(dedent.Dedent(`
	(
	-- before 0
	0, -- after 0
	-- before
	1, -- after
	-- before 3
	3 -- after 3
	)
	`), lexer.Write(a))
}
