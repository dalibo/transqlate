package ast_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestLeafIn(t *testing.T) {
	r := require.New(t)
	leaf := ast.Leaf{Token: lexer.Token{Str: "1"}}
	other := ast.Leaf{Token: lexer.Token{Str: "1"}}
	r.True(leaf.In(other))
	r.True(leaf.In("1", "2"))
	r.False(leaf.In("2"))
}
