package ast

import (
	"slices"
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
)

// Prefix is a node with an operator with a single operand after.
// e.g. NOT 1, -2
//
// Prefix is used to hold item of AND list in [ast.Where].
type Prefix struct {
	Token      lexer.Token
	Expression Node
}

func (p Prefix) IsZero() bool {
	return p.Token.Type == lexer.Void && p.Expression == nil
}

func (p Prefix) Tokens() []lexer.Token {
	t := []lexer.Token{p.Token}
	if p.Expression != nil {
		t = append(t, p.Expression.Tokens()...)
	}
	return t
}

func (p Prefix) TransformStart(f Transformer[lexer.Token]) Node {
	if p.Token.IsZero() {
		p.Expression = p.Expression.TransformStart(f)
	} else {
		p.Token = f(p.Token)
	}
	return p
}

func (p Prefix) TransformEnd(f Transformer[lexer.Token]) Node {
	p.Expression = p.Expression.TransformEnd(f)
	return p
}

func (p Prefix) Visit(f Transformer[Node]) Node {
	if p.Expression != nil {
		p.Expression = p.Expression.Visit(f)
		if p.Expression == nil {
			return nil
		}
	}
	return f(p)
}

func (p Prefix) indent(indentation string) Node {
	if p.Token.Type == lexer.Void {
		p.Expression = Indent(p.Expression, indentation)
		return p
	}
	if p.Token.Type == lexer.Keyword {
		p.Token = p.Token.LeftIndent(indentation)
	} else {
		p.Token = p.Token.Indent(indentation)
	}
	if p.Token.Type != lexer.Keyword {
		indentation = lexer.IncreaseIndentation(indentation, p.Token.Raw)
	}
	p.Expression = OneSpace(Indent(p.Expression, indentation))
	return p
}

// Is reports whether operator of the Infix node matches a list of strings.
func (i Prefix) Is(s string) bool {
	return i.Token.Str == s || s == ""
}

// SetOperator sets the operator of the prefix node.
//
// Move spacings before operator.
func (p Prefix) SetOperator(t lexer.Token) Prefix {
	p.Token = t
	p.Expression = p.Expression.TransformStart(func(t lexer.Token) lexer.Token {
		p.Token.Prefix = t.Prefix
		t.Prefix = " "
		return t
	})
	return p
}

// RemoveOperator removes the operator of the prefix node.
//
// Move operator prefix to expression.
func (p Prefix) RemoveOperator() Prefix {
	p.Expression = p.Expression.TransformStart(func(t lexer.Token) lexer.Token {
		t.Prefix = p.Token.Prefix
		return t
	})
	p.Token = lexer.Token{}
	return p
}

// Infix is a node with an operator with left and right operands.
// e.g. 1 + 2, 3 OR 4
type Infix struct {
	Left  Node
	Op    []lexer.Token
	Right Node
}

func (i Infix) Tokens() []lexer.Token {
	t := []lexer.Token{}
	t = append(t, i.Left.Tokens()...)
	t = append(t, i.Op...)
	t = append(t, i.Right.Tokens()...)
	return t
}

func (i Infix) TransformStart(f Transformer[lexer.Token]) Node {
	i.Left = i.Left.TransformStart(f)
	return i
}

func (i Infix) TransformEnd(f Transformer[lexer.Token]) Node {
	i.Right = i.Right.TransformEnd(f)
	return i
}

func (i Infix) Visit(f Transformer[Node]) Node {
	i.Left = i.Left.Visit(f)
	i.Right = i.Right.Visit(f)
	if i.Left == nil {
		return i.Right
	}
	if i.Right == nil {
		return i.Left
	}
	return f(i)
}

func (i Infix) indent(indentation string) Node {
	i.Left = Indent(i.Left, indentation)
	_, before := lexer.Edges(i.Left)

	isUnion := slices.Contains([]string{"UNION", "INTERSECT", "EXCEPT"}, i.Op[0].Str)
	river := indentation
	if isUnion {
		i.Left = i.Left.TransformEnd(func(t lexer.Token) lexer.Token {
			before = t.EndLine()
			return before
		})
		river = lexer.IncreaseIndentation(river, "SELECT")
	}

	if before.EndsLine() {
		if i.Op[0].Type == lexer.Keyword {
			i.Op[0] = i.Op[0].LeftIndent(river)
		} else {
			i.Op[0] = i.Op[0].Indent(indentation)
		}
	} else if i.Op[0].Str != "." {
		i.Op[0].Prefix = " "
	}

	// Wrap UNION with empty lines. See https://www.sqlstyle.guide/fr/#espacement-des-lignes.
	if isUnion {
		if !strings.Contains(i.Op[0].Prefix, "\n") {
			// Prepend new line.
			i.Op[0].Prefix = "\n" + i.Op[0].Prefix
		}
		// Append new line after UNION
		l := len(i.Op)
		i.Op[l-1] = i.Op[l-1].EndLine()

		// Prepend new line before right.
		i.Right = i.Right.TransformStart(func(t lexer.Token) lexer.Token {
			if !strings.HasPrefix(t.Prefix, "\n") {
				t.Prefix = "\n" + t.Prefix
			}
			return t
		})
	}

	before = i.Op[len(i.Op)-1]
	i.Right, _ = IndentAfter(before, i.Right, indentation)
	return i
}

// Is reports whether operator of the Infix node matches a list of strings.
func (i Infix) Is(s ...string) bool {
	if len(i.Op) != len(s) {
		return false

	}
	for index, op := range i.Op {
		if op.Str != s[index] || s[index] == "" {
			return false
		}
	}
	return true
}

// Rebalance ensure tip of Infix chain of same operator starts with first item.
// Assumes that Left is the chain of previous items.
func (i Infix) Rebalance() Node {
	l, ok := i.Left.(Infix)
	if !ok {
		// There is no previous infix, return self.
		return i
	}
	if !l.sameOperator(i) {
		// The previous infix is another operator, not a chain, return self.
		return i
	}

	// The previous infix is the same operator, rebalance the chain.
	i.Left = l.Right
	l.Right = i

	// l is the new tip of the chain, recurse.
	return l.Rebalance()
}

func (i Infix) sameOperator(o Infix) bool {
	for index, op := range i.Op {
		if index+1 > len(o.Op) {
			return false
		}
		if op.Str != o.Op[index].Str {
			return false
		}
	}
	return true
}

// Postfix is a node with a single operator and a left operand.
// e.g. mytable.column(+)
//
// Also used as an item of comma separated list. The comma is stored in the Token field.
type Postfix struct {
	Expression Node
	Token      lexer.Token
}

func (p Postfix) Tokens() []lexer.Token {
	return append(p.Expression.Tokens(), p.Token)
}

func (p Postfix) TransformStart(f Transformer[lexer.Token]) Node {
	p.Expression = p.Expression.TransformStart(f)
	return p
}

func (p Postfix) TransformEnd(f Transformer[lexer.Token]) Node {
	if p.Token.IsZero() {
		p.Expression = p.Expression.TransformEnd(f)
	} else {
		p.Token = f(p.Token)
	}
	return p
}

func (p Postfix) indent(indentation string) Node {
	p.Expression = Indent(p.Expression, indentation)
	return p
}

func (p Postfix) Visit(f Transformer[Node]) Node {
	p.Expression = p.Expression.Visit(f)
	if p.Expression == nil {
		return nil
	}
	return f(p)
}

// Is reports whether the operator is one string.
func (p Postfix) Is(s string) bool {
	return p.Token.Str == s
}

// SetOperator sets the operator of the postfix node.
//
// Move spacings after operator.
func (p Postfix) SetOperator(t lexer.Token) Postfix {
	_, end := lexer.Edges(p.Expression)
	p.Expression = p.Expression.TransformEnd(func(t lexer.Token) lexer.Token {
		t.Suffix = ""
		return t
	})
	t.Suffix = end.Suffix
	p.Token = t
	return p
}

// RemoveOperator removes the operator of the postfix node.
//
// Move spacings before operator.
func (p Postfix) RemoveOperator() Postfix {
	p.Expression = p.Expression.TransformStart(func(t lexer.Token) lexer.Token {
		t.Suffix = p.Token.Suffix
		return t
	})
	p.Token = lexer.Token{}
	return p
}

// LiteralCast is a node with a type and a string literal.
// e.g. date '2020-01-01' or interval '1 day'
type LiteralCast struct {
	Type    lexer.Token
	Literal lexer.Token
}

func (c LiteralCast) Tokens() []lexer.Token {
	t := []lexer.Token{}
	t = append(t, c.Type)
	t = append(t, c.Literal)
	return t
}

func (c LiteralCast) TransformStart(f Transformer[lexer.Token]) Node {
	c.Type = f(c.Type)
	return c
}

func (c LiteralCast) TransformEnd(f Transformer[lexer.Token]) Node {
	c.Literal = f(c.Literal)
	return c
}

func (c LiteralCast) Visit(f Transformer[Node]) Node {
	return f(c)
}
