package ast_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/internal/parser"
)

func TestPrefixIsZero(t *testing.T) {
	r := require.New(t)

	prefix := ast.Prefix{}
	r.True(prefix.IsZero())

	prefix.Expression = parser.MustParse("1")
	r.False(prefix.IsZero())

	prefix.Expression = nil
	r.True(prefix.IsZero())

	prefix.Token = lexer.MustLex("NOT")[0]
	r.False(prefix.IsZero())
}

func TestInfixIs(t *testing.T) {
	r := require.New(t)
	operation := parser.MustParse("SELECT 1 MINUS SELECT 2").(ast.Infix)
	r.True(operation.Is("MINUS"))
}

func TestInfixChainRebalance(t *testing.T) {
	r := require.New(t)
	and := lexer.MustLex(" AND")
	plus := lexer.MustLex(" +")

	// Tree for 1 + 2 AND 3 AND 4 AND 5
	l := ast.Infix{
		Left: ast.Infix{
			Left: ast.Infix{
				Left: ast.Infix{
					Left:  parser.MustParse("1"),
					Op:    plus,
					Right: parser.MustParse(" 2"),
				},
				Op:    and,
				Right: parser.MustParse(" 3"),
			},
			Op:    and,
			Right: parser.MustParse(" 4"),
		},
		Op:    and,
		Right: parser.MustParse(" 5"),
	}
	r.Equal("1 + 2 AND 3 AND 4 AND 5", lexer.Write(l))
	n := l.Rebalance()

	wanted := ast.Infix{
		Left: ast.Infix{
			Left:  parser.MustParse("1"),
			Op:    plus,
			Right: parser.MustParse(" 2"),
		},
		Op: and,
		Right: ast.Infix{
			Left: parser.MustParse(" 3"),
			Op:   and,
			Right: ast.Infix{
				Left:  parser.MustParse(" 4"),
				Op:    and,
				Right: parser.MustParse(" 5"),
			},
		},
	}

	r.Equal(wanted, n)
	r.Equal("1 + 2 AND 3 AND 4 AND 5", lexer.Write(n))
}

func TestIndentUnion(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT 1 UNION SELECT 2")
	r.Equal(dedent.Dedent(`
	SELECT 1

	 UNION

	SELECT 2`)[1:], lexer.Write(ast.Indent(n, "")))
}
