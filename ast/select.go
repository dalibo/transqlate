package ast

import (
	"gitlab.com/dalibo/transqlate/lexer"
)

// Select holds a SELECT query.
type Select struct {
	With      With
	Select    []lexer.Token
	Distinct  Clause
	List      Items
	From      From
	Where     Where
	Hierarchy Node // May be an [ast.ConnectBy] or [ast.StartWith]
	GroupBy   Node // May be an [ast.Having] for Oracle HAVING before GROUP BY.
	OrderBy   Node
	Limit     Node // May be [ast.Limit], [ast.Offset] or [ast.Fetch]
}

// IsStar reports whether SELECT returns *.
//
// It's use to determine whether the FROM order is important.
func (s Select) IsStar() bool {
	if len(s.List) > 1 {
		return false
	}
	leaf, _ := s.List[0].Expression.(Leaf)
	return leaf.In("*")
}

func (s Select) indent(indentation string) Node {
	if !s.With.IsZero() {
		s.With = Indent(s.With, indentation)
	}
	s.Select[0] = s.Select[0].Indent(indentation)
	if len(s.Select) == 1 && s.Select[0].Suffix == "\n" {
		s.Select[0].Suffix = ""
	}
	river := lexer.IncreaseIndentation(indentation, s.Select[0].Raw)
	before := s.Select[len(s.Select)-1]
	s.List, before = IndentAfter(before, s.List, river)
	if !s.From.IsZero() {
		s.From, before = IndentAfter(before, s.From, river)
	}
	if !s.Where.IsZero() {
		s.Where, before = IndentAfter(before, s.Where, river)
	}
	if s.Hierarchy != nil {
		s.Hierarchy, before = IndentAfter(before, s.Hierarchy, river)
	}
	if s.GroupBy != nil {
		s.GroupBy, before = IndentAfter(before, s.GroupBy, river)
	}
	if s.OrderBy != nil {
		s.OrderBy, before = IndentAfter(before, s.OrderBy, river)
	}
	if s.Limit != nil {
		s.Limit, _ = IndentAfter(before, s.Limit, river)
	}

	return s
}

func (s Select) Tokens() []lexer.Token {
	var t []lexer.Token
	if !s.With.IsZero() {
		t = append(t, s.With.Tokens()...)
	}
	t = append(t, s.Select...)
	if !s.Distinct.IsZero() {
		t = append(t, s.Distinct.Tokens()...)
	}
	if s.List != nil {
		t = append(t, s.List.Tokens()...)
	}
	if !s.From.IsZero() {
		t = append(t, s.From.Tokens()...)
	}
	if !s.Where.IsZero() {
		t = append(t, s.Where.Tokens()...)
	}
	if s.Hierarchy != nil {
		t = append(t, s.Hierarchy.Tokens()...)
	}
	if s.GroupBy != nil {
		t = append(t, s.GroupBy.Tokens()...)
	}
	if s.OrderBy != nil {
		t = append(t, s.OrderBy.Tokens()...)
	}
	if s.Limit != nil {
		t = append(t, s.Limit.Tokens()...)
	}
	return t
}

func (s Select) TransformStart(f Transformer[lexer.Token]) Node {
	if !s.With.IsZero() {
		s.With = s.With.TransformStart(f).(With)
	} else {
		s.Select[0] = f(s.Select[0])
	}
	return s
}

func (s Select) TransformEnd(f Transformer[lexer.Token]) Node {
	if s.Limit != nil {
		s.Limit = s.Limit.TransformEnd(f)
	} else if s.OrderBy != nil {
		s.OrderBy = s.OrderBy.TransformEnd(f)
	} else if s.GroupBy != nil {
		s.GroupBy = s.GroupBy.TransformEnd(f)
	} else if s.Hierarchy != nil {
		s.Hierarchy = s.Hierarchy.TransformEnd(f)
	} else if !s.Where.IsZero() {
		s.Where = s.Where.TransformEnd(f).(Where)
	} else if !s.From.IsZero() {
		s.From = s.From.TransformEnd(f).(From)
	} else {
		s.List = s.List.TransformEnd(f).(Items)
	}
	return s
}

func (s Select) Visit(f Transformer[Node]) Node {
	if !s.With.IsZero() {
		notNil(s.With.Visit(f), &s.With)
	}

	if !s.Distinct.IsZero() {
		notNil(s.Distinct.Visit(f), &s.Distinct)
		if s.Distinct.IsZero() {
			s.Select = s.Select[:1] // Remove DISTINCT keyword
		}
	}

	s.List = s.List.Visit(f).(Items)
	if s.List == nil {
		return nil
	}
	if !s.From.IsZero() {
		notNil(s.From.Visit(f), &s.From)
	}
	if !s.Where.IsZero() {
		notNil(s.Where.Visit(f), &s.Where)
	}
	if s.Hierarchy != nil {
		s.Hierarchy = s.Hierarchy.Visit(f)
	}
	if s.GroupBy != nil {
		s.GroupBy = s.GroupBy.Visit(f)
	}
	if s.OrderBy != nil {
		s.OrderBy = s.OrderBy.Visit(f)
	}
	if s.Limit != nil {
		s.Limit = s.Limit.Visit(f)
	}
	return f(s)
}

// ColumnNames lists the names of the columns in the SELECT clause.
func (s Select) ColumnNames() (names []string) {
	for _, n := range s.List {
		names = append(names, ColumnName(n.Expression))
	}
	return
}

// ColumnName returns the identifier of a column expression.
//
// e.g. "col0" for "a AS col0", "t1.col0", etc.
func ColumnName(n Node) string {
	switch n := n.(type) {
	case Alias:
		return n.Name.Str
	case Infix:
		if !n.Is(".") {
			return ""
		}
		return ColumnName(n.Right)
	case Leaf:
		if n.In("*") {
			return "*"
		}
		if n.Token.Type != lexer.Identifier {
			return ""
		}
		return n.Token.Str
	default:
		return ""
	}
}

// GroupBy holds a GROUP BY clause.
type GroupBy struct {
	GroupBy     []lexer.Token // May contains SIBLINGS
	Expressions Items
	Having      Node
}

func (g GroupBy) Tokens() []lexer.Token {
	t := []lexer.Token{}
	t = append(t, g.GroupBy...)
	t = append(t, g.Expressions.Tokens()...)
	if g.Having != nil {
		t = append(t, g.Having.Tokens()...)
	}
	return t
}

func (g GroupBy) TransformStart(f Transformer[lexer.Token]) Node {
	g.GroupBy[0] = f(g.GroupBy[0])
	return g
}

func (g GroupBy) TransformEnd(f Transformer[lexer.Token]) Node {
	if g.Having == nil {
		g.Expressions = g.Expressions.TransformEnd(f).(Items)
	} else {
		g.Having = g.Having.TransformEnd(f)
	}
	return g
}

func (g GroupBy) Visit(f Transformer[Node]) Node {
	g.Expressions = g.Expressions.Visit(f).(Items)
	if g.Having != nil {
		g.Having = g.Having.Visit(f)
	}
	return f(g)
}

// Having holds a HAVING SQL statement.
type Having struct {
	Having    lexer.Token
	Condition Node
	// Oracle accepts HAVING before GROUP BY.
	// See. https://docs.oracle.com/cd/E11882_01/server.112/e41084/statements_10002.htm#i2065777
	GroupBy Node
}

func (h Having) Tokens() []lexer.Token {
	t := []lexer.Token{h.Having}
	t = append(t, h.Condition.Tokens()...)
	if h.GroupBy != nil {
		t = append(t, h.GroupBy.Tokens()...)
	}
	return t
}

func (h Having) TransformStart(f Transformer[lexer.Token]) Node {
	h.Having = f(h.Having)
	return h
}

func (h Having) TransformEnd(f Transformer[lexer.Token]) Node {
	if h.GroupBy == nil {
		h.Condition = h.Condition.TransformEnd(f)
	} else {
		h.GroupBy = h.GroupBy.TransformEnd(f)
	}
	return h
}

func (h Having) Visit(f Transformer[Node]) Node {
	h.Condition = h.Condition.Visit(f)
	if h.GroupBy != nil {
		h.GroupBy = h.GroupBy.Visit(f)
	}
	return f(h)
}

type OrderBy struct {
	OrderBy  []lexer.Token // May hold SIBLINGS
	Criteria Items
}

func (o OrderBy) Tokens() []lexer.Token {
	t := o.OrderBy
	t = append(t, o.Criteria.Tokens()...)
	return t
}

func (o OrderBy) TransformStart(f Transformer[lexer.Token]) Node {
	o.OrderBy[0] = f(o.OrderBy[0])
	return o
}

func (o OrderBy) TransformEnd(f Transformer[lexer.Token]) Node {
	o.Criteria = o.Criteria.TransformEnd(f).(Items)
	return o
}

func (o OrderBy) Visit(f Transformer[Node]) Node {
	notNil(o.Criteria.Visit(f), &o.Criteria)
	if len(o.Criteria) == 0 {
		return nil
	}
	return f(o)
}

type OrderCriterium struct {
	Expression Node
	// { ASC | DESC | USING operator } [ NULLS { FIRST | LAST } ]
	Tail []lexer.Token
}

func (o OrderCriterium) Tokens() []lexer.Token {
	t := o.Expression.Tokens()
	t = append(t, o.Tail...)
	return t
}

func (o OrderCriterium) TransformStart(f Transformer[lexer.Token]) Node {
	o.Expression = o.Expression.TransformStart(f)
	return o
}

func (o OrderCriterium) TransformEnd(f Transformer[lexer.Token]) Node {
	if len(o.Tail) == 0 {
		o.Expression = o.Expression.TransformEnd(f)
	} else {
		last := len(o.Tail) - 1
		o.Tail[last] = f(o.Tail[last])
	}
	return o
}

func (o OrderCriterium) Visit(f Transformer[Node]) Node {
	o.Expression = o.Expression.Visit(f)
	if o.Expression == nil {
		return nil
	}
	return f(o)
}

// Limit holds a LIMIT clause.
type Limit struct {
	Limit  lexer.Token
	Count  Node
	Offset Node
}

func (l Limit) Tokens() []lexer.Token {
	t := []lexer.Token{l.Limit}
	t = append(t, l.Count.Tokens()...)
	if l.Offset != nil {
		t = append(t, l.Offset.Tokens()...)
	}
	return t
}

func (l Limit) TransformStart(f Transformer[lexer.Token]) Node {
	l.Limit = f(l.Limit)
	return l
}

func (l Limit) TransformEnd(f Transformer[lexer.Token]) Node {
	if l.Offset == nil {
		l.Count = l.Count.TransformEnd(f)
	} else {
		l.Offset = l.Offset.TransformEnd(f)
	}
	return l
}

func (l Limit) Visit(f Transformer[Node]) Node {
	l.Count = l.Count.Visit(f)
	if l.Offset != nil {
		l.Offset = l.Offset.Visit(f)
	}
	return f(l)
}

// Offset holds OFFSET clause of LIMIT.
type Offset struct {
	Offset lexer.Token
	Start  Node
	Unit   lexer.Token // ROW or ROWS
	Fetch  Node
}

func (o Offset) Tokens() []lexer.Token {
	t := []lexer.Token{o.Offset}
	t = append(t, o.Start.Tokens()...)
	if o.Unit.Type != lexer.Void {
		t = append(t, o.Unit)
	}
	if o.Fetch != nil {
		t = append(t, o.Fetch.Tokens()...)
	}
	return t
}

func (o Offset) TransformStart(f Transformer[lexer.Token]) Node {
	o.Offset = f(o.Offset)
	return o
}

func (o Offset) TransformEnd(f Transformer[lexer.Token]) Node {
	if o.Fetch != nil {
		o.Fetch = o.Fetch.TransformEnd(f)
	} else if !o.Unit.IsZero() {
		o.Unit = f(o.Unit)
	} else {
		o.Start = o.Start.TransformEnd(f)
	}
	return o
}

func (o Offset) Visit(f Transformer[Node]) Node {
	o.Start = o.Start.Visit(f)
	if o.Fetch != nil {
		o.Fetch = o.Fetch.Visit(f)
	}
	return f(o)
}

// Fetch holds FETCH FIRST or FETCH NEXT.
type Fetch struct {
	Fetch  []lexer.Token // FETCH { FIRST | NEXT }
	Count  Node
	Tail   []lexer.Token //{ ROW | ROWS } { ONLY | WITH TIES }
	Offset Node          // PostgreSQL accepts OFFSET after FETCH
}

func (f Fetch) Tokens() []lexer.Token {
	t := f.Fetch
	t = append(t, f.Count.Tokens()...)
	t = append(t, f.Tail...)
	if f.Offset != nil {
		t = append(t, f.Offset.Tokens()...)
	}
	return t
}

func (f Fetch) TransformStart(g Transformer[lexer.Token]) Node {
	f.Fetch[0] = g(f.Fetch[0])
	return f
}

func (f Fetch) TransformEnd(g Transformer[lexer.Token]) Node {
	if f.Offset != nil {
		f.Offset = f.Offset.TransformEnd(g)
	} else if len(f.Tail) > 0 {
		last := len(f.Tail) - 1
		f.Tail[last] = g(f.Tail[last])
	} else {
		f.Count = f.Count.TransformEnd(g)
	}
	return f
}

func (f Fetch) Visit(g Transformer[Node]) Node {
	f.Count = f.Count.Visit(g)
	if f.Offset != nil {
		f.Offset = f.Offset.Visit(g)
	}
	return g(f)
}
