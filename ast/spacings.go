package ast

import (
	"fmt"
	"log/slog"
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
)

// SpaceFrom apply spacings from a node to another.
//
// Preserves whitespace, indentation and comments from old node.
func SpaceFrom[T Node](new T, old Node) T {
	start, end := lexer.Edges(old)
	new = new.TransformStart(func(t lexer.Token) lexer.Token {
		t.Prefix = start.Prefix
		return t
	}).(T)
	new = new.TransformEnd(func(t lexer.Token) lexer.Token {
		t.Suffix = end.Suffix
		return t
	}).(T)
	return new
}

// SwapWhitespaces apply spacings from a node to another and vice versa.
//
// Preserves comments. Useful when you swap two nodes.
func SwapWhitespaces[T1 Node, T2 Node](first T1, second T2) (T1, T2) {
	start1, end1 := lexer.Edges(first)
	start2, end2 := lexer.Edges(second)
	first = first.TransformStart(func(t lexer.Token) lexer.Token {
		com := start1.LeadingComment()
		if com == "" {
			if start2.LeadingComment() == "" {
				t.Prefix = start2.Prefix
			} else {
				t.Prefix = start2.Indentation()
			}
		} else {
			t.Prefix = com + start2.Indentation()
		}
		return t
	}).(T1)
	first = first.TransformEnd(func(t lexer.Token) lexer.Token {
		t.Suffix = end2.Outdentation()
		com := end1.TrailingComment()
		if com != "" {
			t.Suffix = strings.TrimRight(t.Suffix, "\n") + com
		}
		return t
	}).(T1)
	second = second.TransformStart(func(t lexer.Token) lexer.Token {
		com := start2.LeadingComment()
		if com == "" {
			if start1.LeadingComment() == "" {
				t.Prefix = start1.Prefix
			} else {
				t.Prefix = start1.Indentation()
			}
		} else {
			t.Prefix = com + start1.Indentation()
		}
		return t
	}).(T2)
	second = second.TransformEnd(func(t lexer.Token) lexer.Token {
		t.Suffix = end1.Outdentation()
		com := end2.TrailingComment()
		if com != "" {
			t.Suffix = strings.TrimRight(t.Suffix, "\n") + com
		}
		return t
	}).(T2)
	return first, second
}

// SwapPrefixes applies prefix from a node to another and vice versa.
//
// It's used when adding a closing parenthesis after a node.
func SwapPrefixes[T1 Node, T2 Node](first T1, second T2) (T1, T2) {
	start1, _ := lexer.Edges(first)
	start2, _ := lexer.Edges(second)
	first = first.TransformStart(func(t lexer.Token) lexer.Token {
		t.Prefix = start2.Prefix
		return t
	}).(T1)
	second = second.TransformStart(func(t lexer.Token) lexer.Token {
		t.Prefix = start1.Prefix
		return t
	}).(T2)
	return first, second
}

// SwapSuffixes applies suffix from a node to another and vice versa.
//
// It's used when adding an opening parenthesis in front of a node.
func SwapSuffixes[T1 Node, T2 Node](first T1, second T2) (T1, T2) {
	_, end1 := lexer.Edges(first)
	_, end2 := lexer.Edges(second)
	first = first.TransformEnd(func(t lexer.Token) lexer.Token {
		t.Suffix = end2.Suffix
		return t
	}).(T1)
	second = second.TransformEnd(func(t lexer.Token) lexer.Token {
		t.Suffix = end1.Suffix
		return t
	}).(T2)
	return first, second
}

// SpaceFromItem apply spacings from an item in a list.
//
// It's used when replacing a list with a plain syntax like decode() with CASE. Since comma will be
// lost, spacings around comma are moved to the new node.
func SpaceFromItem[T Node](new T, items Items, pos int) T {
	comma := items[pos].Token
	if comma.Suffix == "" {
		new = SpaceFrom(new, items[pos].Expression)
	} else {
		new = SpaceFrom(new, items[pos])
	}
	return new
}

// OneSpace prefixes node with a single space.
//
// Use it when moving an expression after a keyword.
// Removes leading comment.
func OneSpace[T Node](n T) T {
	return n.TransformStart(func(t lexer.Token) lexer.Token {
		t.Prefix = " "
		return t
	}).(T)
}

// EndLine ensures node ends line.
func EndLine[T Node](n T) T {
	return n.TransformEnd(func(t lexer.Token) lexer.Token {
		return t.EndLine()
	}).(T)
}

// TrimNewLine removes final new line from node.
//
// Use it when wrapping a node in another.
func TrimNewLine[T Node](n T) T {
	return n.TransformEnd(func(t lexer.Token) lexer.Token {
		return t.TrimNewLine()
	}).(T)
}

type indenter interface {
	indent(string) Node
}

// Indent a whole tree.
//
// Replace node indentation with given string.
// Indentation is recursive.
// Follows Simon Holywell style guide.
func Indent[T Node](n T, i string) T {
	indenter, ok := any(n).(indenter)
	if ok {
		return indenter.indent(i).(T)
	}

	switch any(n).(type) {
	case LiteralCast:
		break
	default:
		slog.Debug("Fallback indent. Wont recurse.", "i", i, "type", fmt.Sprintf("%T", n), "node", lexer.ShortWrite(n))
	}

	return n.TransformStart(func(t lexer.Token) lexer.Token {
		if t.Type == lexer.Keyword {
			// Put starting keyword before the river.
			return t.LeftIndent(i)
		} else {
			return t.Indent(i)
		}
	}).(T)
}

// IndentFrom copies indentation from a node to another.
//
// If old does not begins line, indent is removed.
func IndentFrom[T Node](new T, old Node) T {
	start, _ := lexer.Edges(old)
	return Indent(new, start.Indentation())
}

// IndentAfter indent node after a token.
//
// Handle spacing between token and node like parenthesis, new line or regular keyword.
func IndentAfter[T Node](before lexer.Token, n T, i string) (T, lexer.Token) {
	// Always indent node to recurse.
	n = Indent(n, i)
	_, end := lexer.Edges(n)
	if before.IsZero() {
		return n, end
	}
	if before.EndsLine() {
		return n, end // Node is on its own line. Good.
	}
	if before.Str != "(" && before.Str != "." {
		return OneSpace(n), end // Node is right after a keyword. Add one space.
	}
	// Node is right after opening, no space needed.
	return n.TransformStart(func(t lexer.Token) lexer.Token {
		if t.Prefix == " " {
			t.Prefix = ""
		} else {
			t.Prefix = strings.TrimPrefix(t.Prefix, t.Indentation())
		}
		return t
	}).(T), end
}
