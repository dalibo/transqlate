package ast_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestSpaceNode(t *testing.T) {
	r := require.New(t)
	old := parser.MustParse("-- before\n1 + 2 -- after\n")
	new := parser.MustParse("3 + 4")
	new = ast.SpaceFrom(new, old)
	r.Equal("-- before\n3 + 4 -- after\n", lexer.Write(new))
}

func TestSwapSpaces(t *testing.T) {
	r := require.New(t)
	// Swap space before/after
	first := parser.MustParse(" 1 + 2")
	second := parser.MustParse("3 + 4 ")
	first, second = ast.SwapWhitespaces(first, second)
	r.Equal("1 + 2 ", lexer.Write(first))
	r.Equal(" 3 + 4", lexer.Write(second))
}

func TestSwapSpacesComments(t *testing.T) {
	r := require.New(t)
	first := parser.MustParse(dedent.Dedent(`
	-- before
		1 + 2-- after
	`))
	second := parser.MustParse(dedent.Dedent(`
	-- before
	3 + 4	-- after
	`))

	first, second = ast.SwapWhitespaces(first, second)

	r.Equal(dedent.Dedent(`
	-- before
	1 + 2	-- after
	`), lexer.Write(first))
	r.Equal(dedent.Dedent(`
	-- before
		3 + 4-- after
	`), lexer.Write(second))
}

func TestCommentFromItem(t *testing.T) {
	r := require.New(t)
	old := parser.MustParse(dedent.Dedent(`
	(
		-- before
		a, -- after
		b,
		c
	)
	`)).(ast.Array)
	new := old.Items[0].Expression
	new = ast.SpaceFromItem(new, old.Items, 0)
	r.Equal("\t-- before\n\ta -- after\n", lexer.Write(new))

	old = parser.MustParse(dedent.Dedent(`
	(
		-- before
		a -- after
		, b
		, c
	)
	`)).(ast.Array)
	new = old.Items[0].Expression
	new = ast.SpaceFromItem(new, old.Items, 0)
	r.Equal("\t-- before\n\ta -- after\n", lexer.Write(new))
}

func TestIndentFromItem(t *testing.T) {
	r := require.New(t)
	old := parser.MustParse(dedent.Dedent(`
	(
		a, b,
		c
	)
	`)).(ast.Array)
	new := parser.MustParse("x")
	new = ast.SpaceFromItem(new, old.Items, 0) // From a
	r.Equal("\tx", lexer.Write(new))

	new = ast.SpaceFromItem(new, old.Items, 1) // From b
	r.Equal(" x\n", lexer.Write(new))
}

func TestSwapPrefixes(t *testing.T) {
	r := require.New(t)
	first := parser.MustParse("-- before\nfunc(a) -- after\n")
	second := parser.MustParse("func(b)")

	first, second = ast.SwapPrefixes(first, second)
	r.Equal("func(a) -- after\n", lexer.Write(first))
	r.Equal("-- before\nfunc(b)", lexer.Write(second))
}

func TestSwapSuffixes(t *testing.T) {
	r := require.New(t)
	first := parser.MustParse("-- before\nfunc(a) -- after\n")
	second := parser.MustParse("func(b)")

	first, second = ast.SwapSuffixes(first, second)
	r.Equal("-- before\nfunc(a)", lexer.Write(first))
	r.Equal("func(b) -- after\n", lexer.Write(second))
}
