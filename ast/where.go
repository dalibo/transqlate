package ast

import (
	"strings"

	"gitlab.com/dalibo/transqlate/lexer"
)

// Where is a SQL WHERE clause.
type Where struct {
	Keyword    lexer.Token // WHERE or ON for JOIN clause.
	Conditions []Prefix
}

func (w Where) IsZero() bool {
	return w.Keyword.IsZero() && len(w.Conditions) == 0
}

func (w Where) Tokens() []lexer.Token {
	if w.IsZero() {
		return nil
	}

	t := []lexer.Token{w.Keyword}
	for _, c := range w.Conditions {
		t = append(t, c.Tokens()...)
	}
	return t
}

func (w Where) TransformStart(f Transformer[lexer.Token]) Node {
	w.Keyword = f(w.Keyword)
	return w
}

func (w Where) TransformEnd(f Transformer[lexer.Token]) Node {
	last := len(w.Conditions) - 1
	w.Conditions[last] = w.Conditions[last].TransformEnd(f).(Prefix)
	return w
}

func (w Where) Visit(f Transformer[Node]) Node {
	conditions := w.Conditions
	w.Conditions = nil
	for _, condition := range conditions {
		expr := condition.Visit(f)
		if expr == nil {
			continue
		}
		w.Conditions = append(w.Conditions, expr.(Prefix))
	}
	if len(w.Conditions) == 0 {
		return nil
	}
	w = f(w).(Where)
	return w
}

func (w Where) indent(indentation string) Node {
	w.Keyword = w.Keyword.LeftIndent(indentation)
	before := w.Keyword
	if before.Str == "ON" {
		// Push the river after <AND >
		indentation = lexer.IncreaseIndentation(indentation, "AND")
	}

	for i, c := range w.Conditions {
		c, before = IndentAfter(before, c, indentation)
		w.Conditions[i] = c
	}
	return w
}

// And appends a condition to WHERE clause.
func (w *Where) And(condition Node) {
	item := Prefix{Expression: condition}
	switch len(w.Conditions) {
	case 0:
		// Put condition after WHERE or on next line.
		item = item.TransformStart(func(t lexer.Token) lexer.Token {
			if strings.Contains(w.Keyword.Suffix, "\n") || strings.Contains(t.Prefix, "\n") {
				// WHERE
				// -- comment
				//   a = b
				w.Keyword = w.Keyword.EndLine()
			} else {
				// WHERE a = b
				t.Prefix = " "
			}
			return t
		}).(Prefix)
	default:
		item = item.SetOperator(lexer.MustLex("  AND")[0])
		item = item.TransformStart(func(t lexer.Token) lexer.Token {
			t.Prefix = t.LeadingComment() + w.Keyword.Indentation() + "  "
			return t
		}).(Prefix)
	}
	w.Conditions = append(w.Conditions, item)
}

// Append adds an existing WHERE item.
//
// Handles whether AND operator must be added or removed.
// Preserves spacings.
func (w *Where) Append(items ...Prefix) {
	for _, item := range items {
		condition := SpaceFrom(item.Expression, item)
		w.And(condition)
	}
}

// Delete removes a condition from WHERE clause.
//
// Ensure AND consistency.
func (w *Where) Delete(index int) (condition Node) {
	condition = SpaceFrom(w.Conditions[index].Expression, w.Conditions[index])
	w.Conditions = append(w.Conditions[:index], w.Conditions[index+1:]...)
	if index != 0 || len(w.Conditions) == 0 {
		return
	}

	// Fixup first condition.
	w.Conditions[0] = w.Conditions[0].RemoveOperator()
	w.Conditions[0] = w.Conditions[0].TransformStart(func(t lexer.Token) lexer.Token {
		if strings.Contains(t.Prefix, "\n") {
			// WHERE
			// -- comment
			//   a = b
			w.Keyword = w.Keyword.EndLine()
		} else {
			// WHERE a = b
			w.Keyword = w.Keyword.TrimNewLine()
			t.Prefix = " "
		}
		return t
	}).(Prefix)
	return
}

// TransformColumnRef applies a transformation to all column names in the WHERE clause.
//
// Node passed to f is guaranteed to be a Leaf with an identifier token or an Infix with qualified column ref.
//
// Example: <col> = 1, <t1.col> = <t2.col>, COALESCE(<column>)
func TransformColumnRef(n Node, f Transformer[Node]) Node {
	// Apply f only on operands of operation or call args. Pass either dot Infix for fully qualified
	// column or a Leaf of identifier for unqualified column.
	n = n.Visit(func(n Node) Node {
		switch n := n.(type) {
		case Between:
			if isColumn(n.Expression) {
				n.Expression = f(n.Expression)
			}
			if isColumn(n.Start) {
				n.Start = f(n.Start)
			}
			if isColumn(n.End) {
				n.End = f(n.End)
			}
			return n
		case Case:
			if isColumn(n.Expression) {
				n.Expression = f(n.Expression)
			}
			return n
		case Prefix:
			if isColumn(n.Expression) {
				n.Expression = f(n.Expression)
			}
			return n
		case Infix:
			if n.Is(".") {
				return n
			}
			if isColumn(n.Left) {
				n.Left = f(n.Left)
			}
			if isColumn(n.Right) {
				n.Right = f(n.Right)
			}
			return n
		case Postfix:
			if isColumn(n.Expression) {
				n.Expression = f(n.Expression)
			}
			return n
		case Array: // Applies to call args.
			for i, arg := range n.Items {
				if isColumn(arg.Expression) {
					n.Items[i].Expression = f(arg.Expression)
				}
			}
			return n
		default:
			return n
		}
	})

	if isColumn(n) {
		n = f(n)
	}
	return n
}

func isColumn(n Node) bool {
	switch n := n.(type) {
	case Infix:
		left, _ := n.Left.(Leaf)
		right, _ := n.Right.(Leaf)
		return left.IsIdentifier() && n.Is(".") && right.IsIdentifier()
	case Leaf:
		return n.IsIdentifier()
	default:
		return false
	}
}
