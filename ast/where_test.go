package ast_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/internal/parser"
)

func TestWhereAppendDelete(t *testing.T) {
	r := require.New(t)

	w := parser.MustParse(dedent.Dedent(`
	SELECT 1
	FROM a
	WHERE 1 = 1 -- after 1
	`)).(ast.Select).Where
	r.Len(w.Conditions, 1)

	w.Delete(0)
	r.Len(w.Conditions, 0)

	w.And(parser.MustParse(dedent.Dedent(`
	2 = 2 -- after 2
	`)[1:]))
	w.And(parser.MustParse(dedent.Dedent(`
	-- before 3
	3 = 3 -- after 3
	`)[1:]))
	w.And(parser.MustParse(dedent.Dedent(`
	4 = 4
	`)[1:]))

	r.Len(w.Conditions, 3)
	r.Equal(dedent.Dedent(`
	WHERE 2 = 2 -- after 2
	-- before 3
	  AND 3 = 3 -- after 3
	  AND 4 = 4
	`)[1:], lexer.Write(w))

	w.Delete(0)

	r.Equal(dedent.Dedent(`
	WHERE
	-- before 3
	  3 = 3 -- after 3
	  AND 4 = 4
	`)[1:], lexer.Write(w))

	w.Delete(0)

	r.Equal(dedent.Dedent(`
	WHERE 4 = 4
	`)[1:], lexer.Write(w))
}

func TestTransformColumn(t *testing.T) {
	r := require.New(t)

	edit := func(sql string) ast.Node {
		n := parser.MustParse(sql)
		return ast.TransformColumnRef(n, func(n ast.Node) ast.Node {
			return ast.SpaceFrom(parser.MustParse("'COLUMN_REF'"), n)
		})
	}

	n := edit("a") // boolean column as condition
	r.Equal("'COLUMN_REF'", lexer.Write(n))

	n = edit("(a)")
	r.Equal("('COLUMN_REF')", lexer.Write(n))

	n = edit("a = 1")
	r.Equal("'COLUMN_REF' = 1", lexer.Write(n))

	n = edit("a = 1 AND b = 2")
	r.Equal("'COLUMN_REF' = 1 AND 'COLUMN_REF' = 2", lexer.Write(n))

	n = edit("a.b = 1")
	r.Equal("'COLUMN_REF' = 1", lexer.Write(n))

	n = edit("COALESCE(a) = 1")
	r.Equal("COALESCE('COLUMN_REF') = 1", lexer.Write(n))

	n = edit("PRIOR (a = 1)")
	r.Equal("PRIOR ('COLUMN_REF' = 1)", lexer.Write(n))

	n = edit("a BETWEEN b.c AND d")
	r.Equal("'COLUMN_REF' BETWEEN 'COLUMN_REF' AND 'COLUMN_REF'", lexer.Write(n))
}
