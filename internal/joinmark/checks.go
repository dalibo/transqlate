package joinmark

import (
	"fmt"
	"log/slog"
	"slices"

	mapset "github.com/deckarep/golang-set/v2"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// hasJoinMark returns whether a node has a join mark.
//
// It is used to check whether a SELECT needs to be rewritten or if a rewritten SELECT is not fully
// translated.
func hasJoinMark(n ast.Node) (marked bool) {
	if n == nil {
		return false
	}
	n.Visit(func(n ast.Node) ast.Node {
		p, _ := n.(ast.Postfix)
		marked = marked || p.Is("(+)")
		return n
	})
	return
}

// checkOriginal accepts a query for rewrite.
//
// Return nil if the query is valid for rewrite or a reason.
func checkOriginal(tableOrder []string, where ast.Node) error {
	tables := listAllReferencedTables(where)
	if slices.Contains(tables, "") {
		return fmt.Errorf("can't guess table from column")
	}
	return nil
}

// checkRewrite ensures the rewrite is consistent.
//
// It checks that all tables are still present and that the SELECT * order is preserved.
// It also checks that all join marks have been converted.
//
// If rewritten is inconsistent, prefer to return orig.
func checkRewrite(orig ast.Node, rewritten ast.Node, keepOrder bool) (ast.Node, error) {
	flattenOrig := orig.(ast.Select).From
	flattenOrig.Tables = flattenOrig.Flatten()
	origOrder := flattenOrig.Names()
	rewrittenOrder := rewritten.(ast.Select).From.Names()
	if !mapset.NewSet(origOrder...).Equal(mapset.NewSet(rewrittenOrder...)) {
		// We lost some tables somewhere. Give up.
		slog.Debug("Inconsistent FROM.", "orig", lexer.Write(orig.(ast.Select).From), "rewritten", lexer.Write(rewritten.(ast.Select).From))
		return orig, fmt.Errorf("failed to rewrite join marks")
	}

	whereCount := countConditions(orig.(ast.Select))
	rewrittenCount := countConditions(rewritten.(ast.Select))
	if whereCount != rewrittenCount {
		slog.Debug("Inconsistent conditions.", "orig", whereCount, "rewritten", rewrittenCount)
		return orig, fmt.Errorf("failed to rewrite join marks")
	}

	if keepOrder && !slices.Equal(origOrder, rewrittenOrder) {
		return rewritten, fmt.Errorf("SELECT * reordered")
	}

	if hasJoinMark(rewritten.(ast.Select).Where) {
		return rewritten, fmt.Errorf("not all join marks converted")
	}

	return rewritten, nil
}

// countConditions returns the number of recognized condition
//
// These conditions are supposed to be moved by the rules. It's used to check whether the rule
// dropped or duplicated a condition while editing the AST.
func countConditions(select_ ast.Select) (count int) {
	var expressions []ast.Node
	expressions = append(expressions, select_.Where)
	for _, item := range select_.From.Tables {
		join, isJoin := item.Expression.(ast.Join)
		if !isJoin {
			continue
		}
		expressions = append(expressions, join.Condition)
	}
	for _, expr := range expressions {
		slog.Debug("Count conditions.", "expr", lexer.Write(expr))
		where, ok := expr.(ast.Where)
		if !ok {
			continue
		}
		count += len(where.Conditions)
	}
	return
}
