package parser

import (
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// parseCall parses a function call.
func (p *state) parseCall(f ast.Node) (ast.Node, error) {
	defer Trace.enter()()

	c := ast.Call{
		Function: f,
		Open:     p.consume(), // Assume opening paren is matched.
	}
	if p.keywords("DISTINCT") {
		c.Distinct = p.consume()
	}
	if p.punctuations(")") {
		c.Close = p.consume()
		return c, nil
	}
	var err error
	c.Args, err = p.parseItems(nil)
	if err != nil {
		return nil, err
	}

	// Parse call clauses like (ORDER BY X SEPARATOR ',')
	cont := true
	for cont {
		switch {
		case p.keywords("AS"), p.keywords("ESCAPE"), p.keywords("FOR"), p.keywords("FROM"), p.keywords("SEPARATOR"), p.keywords("SIMILAR"):
			clause, err := p.parsePrefix(0)
			if err != nil {
				return c, err
			}
			c.Clauses = append(c.Clauses, clause)
		case p.keywords("ORDER", "BY"):
			clause, err := p.parseOrderBy()
			if err != nil {
				return c, err
			}
			c.Clauses = append(c.Clauses, clause)
		case p.keywords("ON", "OVERFLOW"):
			// Since we will delete thise clause,
			// just put it in a generic Expression node.
			clause := ast.Expression(p.consumeMatched())
			if p.keywords("ERROR") {
				clause = append(clause, p.consume())
				c.Clauses = append(c.Clauses, clause)
				continue
			} else if !p.keywords("TRUNCATE") {
				return nil, p.unexpected()
			}
			clause = append(clause, p.consume())
			if !p.seek(lexer.String) {
				return nil, p.unexpected()
			}
			clause = append(clause, p.consume())
			// WITH|WITHOUT COUNT
			if p.keywords("WITH", "COUNT") || p.keywords("WITHOUT", "COUNT") {
				clause = append(clause, p.consumeMatched()...)
			}
			c.Clauses = append(c.Clauses, clause)
		default:
			cont = false
		}
	}
	if !p.punctuations(")") {
		return nil, p.unexpected()
	}
	c.Close = p.consume()

	if p.keywords("WITHIN", "GROUP") || p.keywords("OVER") {
		return p.parseAggregate(c)
	}

	return c, nil
}

func (p *state) parseAggregate(call ast.Call) (ast.Node, error) {
	agg := ast.Aggregate{
		Call: call,
	}
	if p.keywords("WITHIN", "GROUP") {
		c := ast.Clause{
			Keywords: p.consumeMatched(),
		}

		e, err := p.parseClauses()
		if err != nil {
			return nil, err
		}
		c.Expression = e
		agg.WithinGroup = c
	}

	if p.keywords("OVER") {
		over := ast.Clause{
			Keywords: p.consumeMatched(),
		}
		e, err := p.parseClauses()
		if err != nil {
			return nil, err
		}
		over.Expression = e
		agg.Over = over
	}

	return agg, nil
}

// parseClauses parse clauses like (ORDER BY X PARTITION BY Y)
//
// Use for parsing aggregate clauses.
// parseCall handle call clauses like `(a, b FROM X TO Y)`.
func (p *state) parseClauses() (ast.Node, error) {
	if !p.punctuations("(") {
		return nil, p.unexpected()
	}

	args := ast.Array{
		Open: p.consume(),
	}

	for !p.punctuations(")") {
		if p.seek(lexer.EOF, "") {
			return nil, p.unexpected()
		}
		switch {
		case p.keywords("ORDER", "BY"):
			orderBy, err := p.parseOrderBy()
			if err != nil {
				return nil, err
			}
			args.Items.Append(orderBy)
		case p.keywords("PARTITION", "BY"):
			clause := ast.Clause{
				Keywords: p.consumeMatched(),
			}
			var err error
			var e ast.Node
			if p.punctuations("(") {
				e, err = p.parseArray()
			} else {
				e, err = p.parseItems(nil)
			}
			if err != nil {
				return nil, err
			}
			clause.Expression = e
			args.Items.Append(clause)
		default:
			return nil, p.unexpected()
		}
	}

	if !p.punctuations(")") {
		return nil, p.unexpected()
	}

	args.Close = p.consume()
	return args, nil
}
