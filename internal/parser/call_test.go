package parser_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestCall(t *testing.T) {
	r := require.New(t)
	n, err := parser.Parse(lexer.New("<stdin>", "foo(1, 2) + 3"))
	r.NoError(err)
	// plus operator is at the top level
	plus := n.(ast.Infix)
	r.Equal("+", plus.Op[0].Raw)

	c := plus.Left.(ast.Call)
	r.True(c.Is("foo"))

	r.Equal("(", c.Open.Raw)
	r.Len(c.Args, 2)
	a := c.Args[0].Expression.(ast.Leaf)
	r.Equal("1", a.Token.Raw)
	a = c.Args[1].Expression.(ast.Leaf)
	r.Equal("2", a.Token.Raw)
	r.Equal(")", c.Close.Raw)

	a = plus.Right.(ast.Leaf)
	r.Equal("3", a.Token.Raw)
}

func TestAggOver(t *testing.T) {
	r := require.New(t)
	agg := parser.MustParse("array_agg(1) OVER (PARTITION BY 2 ORDER BY 3)").(ast.Aggregate)
	r.True(agg.Call.Is("array_agg"))
	r.Nil(agg.WithinGroup)
	over := agg.Over.(ast.Clause)
	clauses := over.Expression.(ast.Array)
	r.Len(clauses.Items, 2)

}

func TestListaggWithinGroup(t *testing.T) {
	r := require.New(t)
	agg := parser.MustParse("LISTAGG(1) WITHIN GROUP (ORDER BY 1)").(ast.Aggregate)
	r.True(agg.Call.Is("listagg"))
	wg := agg.WithinGroup.(ast.Clause)
	clauses := wg.Expression.(ast.Array)
	r.Len(clauses.Items, 1)
	r.Nil(agg.Over)
}
