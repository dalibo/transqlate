// Package parser contains the universal TDOP parser for SQL snippet.
//
// Accepts partial SQL statements like DEFAUT values for columns.
// Parses a mix of all SQL dialects.
// Returns a parse tree of nodes from [ast].
package parser
