package parser

import (
	"fmt"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// parseExpression parses a single expression.
// An expression is either a query, an operation, a literal or a function call.
// Nested operations are handled by the precedence.
func (p *state) parseExpression(precedence int) (expr ast.Node, err error) {
	defer Trace.enterf("precedence=%d", precedence)()

	switch {
	case p.seek(lexer.EOF):
		err = p.errorf("unexpected end of file")
	case p.keywords("CASE"):
		expr, err = p.parseCase()
		if err != nil {
			err = p.errorf("case: %w", err)
		}
	case p.operators("+", "-"), p.keywords("DISTINCT"), p.keywords("NOT"), p.keywords("EXISTS"), p.keywords("PRIOR"), p.keywords("TABLE"):
		expr, err = p.parsePrefix(50)
	case p.punctuations("*"), p.keywords("NULL"), p.keywords("TRUE"), p.keywords("FALSE"):
		expr = p.parseLeaf()
	case p.keywords("WITH"), p.keywords("SELECT"):
		expr, err = p.parseSelect()
		if err != nil {
			err = p.errorf("select: %w", err)
		}
	case p.keywords("SHOW"):
		return p.parseGeneric(), nil
	case p.seek(lexer.Identifier):
		expr = p.parseLiteralCast()
		if expr == nil {
			expr = p.parseLeaf()
		}
	case p.seek(lexer.String), p.seek(lexer.Integer), p.seek(lexer.Float):
		expr = p.parseLeaf()
	case p.punctuations("(", "["):
		expr, err = p.parseArray()
	default:
		return nil, p.unexpected()
	}

	if err != nil {
		return nil, err
	}

	// Try to find an operation after parsed expression.
	for {
		parserFunc, nextPrecedence := p.lookaheadOperator()
		if precedence >= nextPrecedence {
			// Next operation has lower precedence,
			// we stop here and let the parent operation handle it.
			// This is the end of the expression.
			break
		}
		// Parse the next operation.
		// This will be the right operand of the current operation.
		// The current operation will be the left operand of the next operation.
		expr, err = parserFunc(expr, nextPrecedence)
		if err != nil {
			return nil, err
		}
	}

	return
}

// parseGeneric wraps all tokens until end of statement
func (p *state) parseGeneric() ast.Node {
	defer Trace.enter()()
	expr := ast.Expression{}
	for !p.seek(lexer.EOF) && !p.punctuations(";") && !p.punctuations(")") {
		expr = append(expr, p.consume())
	}
	return expr
}

// parseAlias parses an alias with or without AS.
//
// String as alias : https://dev.mysql.com/doc/refman/8.0/en/problems-with-alias.html
func (p *state) parseAlias(expr ast.Node, _ int) (ast.Node, error) {
	defer Trace.enter()()
	a := ast.Alias{
		Expression: expr,
	}

	if p.keywords("AS") {
		a.As = p.consume()
	}

	if !p.seek(lexer.Identifier) && !p.seek(lexer.String) {
		return nil, fmt.Errorf("expected identifier, got %s", p.tokens[p.pos])
	}
	a.Name = p.consume()
	return a, nil
}

// parseLiteralCast parses a string literal with its cast.
func (p *state) parseLiteralCast() ast.Node {
	// Assume an identifier is matched.
	// Search a literal string after identifier.
	if !p.seekAhead(lexer.String) {
		return nil
	}
	defer Trace.enter()() // Trace only if matched.
	tokens := p.consumeMatched()
	return ast.LiteralCast{
		Type:    tokens[0],
		Literal: tokens[1],
	}
}

// parseLeaf parses an identifier, a keyword or a literal.
func (p *state) parseLeaf() ast.Node {
	defer Trace.enter()()

	return ast.Leaf{
		Token: p.consume(),
	}
}

func (p *state) parseBetween(expr ast.Node) (ast.Node, error) {
	defer Trace.enter()()

	b := ast.Between{
		Expression: expr,
		// Assume BETWEEN or NOT BETWEEN is already matched.
		Between: p.consumeMatched(),
	}
	expr, err := p.parseExpression(betweenPrec)
	if err != nil {
		return nil, fmt.Errorf("range: %w", err)
	}
	b.Start = expr
	if !p.keywords("AND") {
		return nil, p.unexpected()
	}
	b.And = p.consume()
	expr, err = p.parseExpression(betweenPrec)
	if err != nil {
		return nil, fmt.Errorf("range: %w", err)
	}
	b.End = expr
	return b, nil
}

// parseCase parses a CASE expression.
func (p *state) parseCase() (ast.Node, error) {
	defer Trace.enter()()

	c := ast.Case{
		Case: p.consume(),
	}
	if !p.keywords("WHEN") {
		expr, err := p.parseExpression(0)
		if err != nil {
			return nil, err
		}
		c.Expression = expr
	}

	for p.keywords("WHEN") {
		w, err := p.parseWhen()
		if err != nil {
			return nil, p.errorf("when: %w", err)
		}
		c.Cases = append(c.Cases, w)
	}

	if p.keywords("ELSE") {
		e, err := p.parsePrefix(0)
		if err != nil {
			return nil, fmt.Errorf("else: %w", err)
		}
		c.Else = e.(ast.Prefix)
	}
	if !p.keywords("END") {
		return nil, p.unexpected()
	}
	c.End = p.consume()
	return c, nil
}

// parseWhen parses a WHEN expression from a CASE.
func (p *state) parseWhen() (ast.When, error) {
	defer Trace.enter()()

	w := ast.When{}
	w.When.Token = p.consume()
	expr, err := p.parseExpression(0)
	if err != nil {
		return w, err
	}
	w.When.Expression = expr
	if !p.keywords("THEN") {
		return w, p.unexpected()
	}
	w.Then.Token = p.consume()
	expr, err = p.parseExpression(0)
	if err != nil {
		return w, err
	}
	w.Then.Expression = expr
	return w, nil
}

var closers = map[string]string{
	"(": ")",
	"[": "]",
}

// parseArray parses wrapped items.
func (p *state) parseArray() (g ast.Array, err error) {
	defer Trace.enter()()

	g.Open = p.consume() // Assume the opening token is already matched.
	closer := closers[g.Open.Str]
	if !p.punctuations(closer) {
		g.Items, err = p.parseItems(nil)
		if err != nil {
			return g, err
		}
	}
	if !p.punctuations(closer) {
		return g, p.unexpected()
	}
	g.Close = p.consume()
	return g, nil
}

// parseItems parses a comma separated list of expressions
//
// If expr is non-nil, it will be used as first item.
func (p *state) parseItems(expr ast.Node) (items ast.Items, err error) {
	defer Trace.enter()()

	for {
		if expr == nil {
			expr, err = p.parseExpression(0)
			if err != nil {
				return
			}
		}

		item := ast.Postfix{Expression: expr}
		expr = nil // Reset for next iteration.
		if p.punctuations(",") {
			item.Token = p.consume()
		}
		items = append(items, item)
		if item.Token.IsZero() {
			break // No comma, break.
		}
	}

	return
}
