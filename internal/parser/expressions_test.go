package parser_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestCommaList(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("1, 2, 3")
	g := n.(ast.Items)
	r.Len(g, 3)

	n, err := parser.Parse(lexer.New("<stdin>", ", 1"))
	r.Error(err)
	r.Nil(n)
}

func TestEmptyParens(t *testing.T) {
	parser.MustParse("()")
}

func TestParens(t *testing.T) {
	r := require.New(t)
	n, err := parser.Parse(lexer.New("<stdin>", "(1 + 2)"))
	r.NoError(err)
	g := n.(ast.Array)
	r.Equal("(", g.Open.Raw)

	n, err = parser.Parse(lexer.New("<stdin>", "(1]"))
	r.Error(err)
	r.Nil(n)
}

func TestParensAsOperand(t *testing.T) {
	r := require.New(t)
	n, err := parser.Parse(lexer.New("<stdin>", "(1), 2, (3, 4)"))
	r.NoError(err)
	g := n.(ast.Items)
	r.Len(g, 3)

	i0 := g[0].Expression.(ast.Array)
	r.Len(i0.Items, 1)
	r.Equal("1", i0.Items[0].Expression.(ast.Leaf).Token.Raw)

	i1 := g[1].Expression.(ast.Leaf)
	r.Equal("2", i1.Token.Raw)

	i2 := g[2].Expression.(ast.Array)
	r.Len(i2.Items, 2)
	r.Equal("3", i2.Items[0].Expression.(ast.Leaf).Token.Raw)
	r.Equal("4", i2.Items[1].Expression.(ast.Leaf).Token.Raw)
}

func TestNestedParens(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("((1 + 2))")
	g := n.(ast.Array)
	r.Equal("(", g.Open.Raw)
	r.Equal(")", g.Close.Raw)
	i := g.Items[0].Expression.(ast.Array)
	r.Equal("(", i.Open.Raw)
	r.Equal(")", i.Close.Raw)
}

func TestBetween(t *testing.T) {
	r := require.New(t)
	between := parser.MustParse("1 BETWEEN 2 AND 3").(ast.Between)
	r.Equal("1", between.Expression.(ast.Leaf).Token.Raw)
	r.Equal("BETWEEN", between.Between[0].Str)
	r.Equal("2", between.Start.(ast.Leaf).Token.Raw)
	r.Equal("AND", between.And.Str)
	r.Equal("3", between.End.(ast.Leaf).Token.Raw)
}

func TestBetweenPrecedence(t *testing.T) {
	r := require.New(t)
	and := parser.MustParse("a BETWEEN b.c AND d(e) AND 4").(ast.Infix)

	r.True(and.Is("AND"))
	between := and.Left.(ast.Between)
	r.Equal("a", between.Expression.(ast.Leaf).Token.Raw)
	r.Equal("BETWEEN", between.Between[0].Str)
	r.Equal(" b.c", lexer.Write(between.Start))
	r.Equal("AND", between.And.Str)
	r.Equal(" d(e)", lexer.Write(between.End))
}

func TestNotBetween(t *testing.T) {
	r := require.New(t)
	between := parser.MustParse("1 NOT BETWEEN 2 AND 3").(ast.Between)
	r.Equal("1", between.Expression.(ast.Leaf).Token.Raw)
	r.Equal("NOT", between.Between[0].Str)
	r.Equal("BETWEEN", between.Between[1].Str)
	r.Equal("2", between.Start.(ast.Leaf).Token.Raw)
	r.Equal("AND", between.And.Str)
	r.Equal("3", between.End.(ast.Leaf).Token.Raw)
}

func TestAliasExplicitAs(t *testing.T) {
	r := require.New(t)
	s := parser.MustParse("SELECT 1 AS foo").(ast.Select)
	a := s.List[0].Expression.(ast.Alias)
	r.Equal("AS", a.As.Str)
	r.Equal("foo", a.Name.Str)
}

func TestAliasImplicitAs(t *testing.T) {
	r := require.New(t)
	s := parser.MustParse("SELECT (1) foo").(ast.Select)
	a := s.List[0].Expression.(ast.Alias)
	r.Equal("", a.As.Str)
	r.Equal("foo", a.Name.Str)
}

func TestCompositeAlias(t *testing.T) {
	r := require.New(t)
	s := parser.MustParse("SELECT 1+2 AS toto").(ast.Select)
	a := s.List[0].Expression.(ast.Alias)
	r.Equal("AS", a.As.Str)
	r.Equal("toto", a.Name.Str)
}

func TestAliasOfAlias(t *testing.T) {
	r := require.New(t)
	// Ensure chained identifiers are not parsed as chain of implicit alias.
	// This way, bare label keywoard are not treated as identifier infinitely.
	n, err := parser.Parse(lexer.New("", "a b c"))
	r.Error(err)
	r.Nil(n)
}

func TestCastFunc(t *testing.T) {
	r := require.New(t)
	n, err := parser.Parse(lexer.New("", "CAST('1' AS int)"))
	r.NoError(err)
	c := n.(ast.Call)
	r.True(c.Is("cast"))

	i := c.Args[0].Expression.(ast.Leaf)
	r.Equal("1", i.Token.Str)
	as := c.Clauses[0].(ast.Prefix)
	r.True(as.Is("AS"))
}

func TestCaseWithExpression(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("CASE 1 WHEN 2 THEN 3 ELSE 4 END")
	c := n.(ast.Case)
	r.Equal("1", c.Expression.(ast.Leaf).Token.Raw)
	r.Len(c.Cases, 1)
	r.Equal("2", c.Cases[0].When.Expression.(ast.Leaf).Token.Raw)
	r.Equal("3", c.Cases[0].Then.Expression.(ast.Leaf).Token.Raw)
	r.Equal("4", c.Else.Expression.(ast.Leaf).Token.Raw)
}

func TestCaseWithoutExpression(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("CASE WHEN 1 THEN 2 WHEN 3 THEN 4 END")
	c := n.(ast.Case)
	r.Nil(c.Expression)
	r.Len(c.Cases, 2)
	r.Equal("1", c.Cases[0].When.Expression.(ast.Leaf).Token.Raw)
	r.Equal("2", c.Cases[0].Then.Expression.(ast.Leaf).Token.Raw)
	r.Equal("3", c.Cases[1].When.Expression.(ast.Leaf).Token.Raw)
	r.Equal("4", c.Cases[1].Then.Expression.(ast.Leaf).Token.Raw)
}

func TestLiteralCast(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("date '2020-01-01'")
	c := n.(ast.LiteralCast)
	r.Equal("date", c.Type.Raw)
	r.Equal("'2020-01-01'", c.Literal.Raw)
}

func TestLiteralCastWithAlias(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT float '1' as foo")
	s := n.(ast.Select)
	c := s.List[0].Expression.(ast.Alias)
	r.Equal("foo", c.Name.Str)
	lc := c.Expression.(ast.LiteralCast)
	r.Equal("float", lc.Type.Str)
	r.Equal("'1'", lc.Literal.Raw)
}

func TestTableFunction(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("TABLE (strings)")
	p := n.(ast.Prefix)
	r.Equal("TABLE", p.Token.Str)
	args := p.Expression.(ast.Array)
	r.Len(args.Items, 1)
}

func TestSubstringBuiltin(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("substring('toto', 1, 2)")
	c := n.(ast.Call)
	r.True(c.Is("substring"))
	r.False(c.Is("SUBSTRING"), "substring is a keyword")
	r.Len(c.Args, 3)
}

func TestSubstringFrom(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("substring('toto' FROM 1 FOR 2)")
	c := n.(ast.Call)
	r.Len(c.Args, 1)
	r.Len(c.Clauses, 2)
}

func TestSubstringSimilar(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("substring('pif' SIMILAR 'paf' ESCAPE 'pouf')")
	c := n.(ast.Call)
	r.Len(c.Args, 1)
	r.Len(c.Clauses, 2)
}

func TestConcat(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("'1' || ('2', '3') || '4'")
	concat := n.(ast.Array)
	r.Len(concat.Items, 3)
}

func TestAggregateClauses(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("GROUP_CONCAT(DISTINCT foo ORDER BY bar SEPARATOR ', ')")
	concat := n.(ast.Call)
	r.Equal("DISTINCT", concat.Distinct.Str)
	r.Len(concat.Args, 1)
	r.Len(concat.Clauses, 2)
}
