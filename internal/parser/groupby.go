package parser

import "gitlab.com/dalibo/transqlate/ast"

// parseGroupBy parses a GROUP BY clause.
func (p *state) parseGroupBy() (ast.Node, error) {
	defer Trace.enter()()
	var g ast.GroupBy

	p.keywords("", "", "ALL")      // Accepts GROUP BY ALL
	p.keywords("", "", "DISTINCT") // Accepts GROUP BY ALL
	g.GroupBy = p.consumeMatched() // assume group by is matched

	// Accept known keywords.
	if p.keywords("CUBE") || p.keywords("ROLLUP") || p.keywords("GROUPING", "SETS") {
		g.GroupBy = append(g.GroupBy, p.consumeMatched()...)
	}

	var err error
	g.Expressions, err = p.parseItems(nil)
	if err != nil {
		return nil, err
	}

	if p.keywords("HAVING") {
		n, err := p.parseHaving()
		if err != nil {
			return nil, err
		}
		g.Having = n
	}

	return g, nil
}

// parseHaving parses a HAVING clause.
func (p *state) parseHaving() (ast.Node, error) {
	defer Trace.enter()()
	havingToken := p.consume()
	c, err := p.parseExpression(0)
	if err != nil {
		return nil, err
	}
	if i, ok := c.(ast.Infix); ok {
		// Ensure first AND/OR is at the top of the tree.
		c = i.Rebalance()
	}
	having := ast.Having{
		Having:    havingToken,
		Condition: c,
	}
	if p.keywords("GROUP", "BY") {
		groupby, err := p.parseGroupBy()
		if err != nil {
			return nil, err
		}
		having.GroupBy = groupby
	}
	return having, nil
}
