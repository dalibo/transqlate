package parser

// C.f. https://docs.oracle.com/cd//B19306_01/server.102/b14200/queries003.htm

import "gitlab.com/dalibo/transqlate/ast"

// parseStartWith
func (p *state) parseStartWith() (ast.Node, error) {
	s := ast.StartWith{
		StartWith: p.consumeMatched(),
	}
	condition, err := p.parseExpression(0)
	if err != nil {
		return nil, err
	}
	s.Condition = condition
	if p.keywords("CONNECT", "BY") {
		s.ConnectBy, err = p.parseConnectBy()
		if err != nil {
			return nil, p.errorf("connect by: %w", err)
		}
	}
	// Assume connect by was parsed before.
	return s, nil
}

// parseConnectBy
func (p *state) parseConnectBy() (ast.Node, error) {
	c := ast.ConnectBy{
		ConnectBy: p.consumeMatched(),
	}
	if p.keywords("NOCYCLE") {
		c.ConnectBy = append(c.ConnectBy, p.consume())
	}
	condition, err := p.parseExpression(0)
	if err != nil {
		return nil, err
	}
	c.Condition = condition

	if p.keywords("START", "WITH") {
		c.StartWith, err = p.parseStartWith()
		if err != nil {
			return nil, p.errorf("start with: %w", err)
		}
	}
	// Assume start with was parsed before.
	return c, nil
}
