package parser

import "gitlab.com/dalibo/transqlate/ast"

func (p *state) parseLimit() (ast.Node, error) {
	defer Trace.enter()()
	limit := ast.Limit{
		Limit: p.consume(),
	}

	if p.keywords("ALL") {
		limit.Count = p.parseLeaf()
	} else {
		count, err := p.parseExpression(0)
		if err != nil {
			return nil, err
		}
		limit.Count = count
	}

	if p.keywords("OFFSET") {
		offset, err := p.parseOffset()
		if err != nil {
			return nil, p.errorf("offset: %w", err)
		}
		limit.Offset = offset
	}

	return limit, nil
}

func (p *state) parseOffset() (ast.Node, error) {
	defer Trace.enter()()
	offset := ast.Offset{
		Offset: p.consume(),
	}

	start, err := p.parseExpression(0)
	if err != nil {
		return nil, err
	}
	offset.Start = start

	if p.keywords("ROW") || p.keywords("ROWS") {
		offset.Unit = p.consume()
	}

	if p.keywords("FETCH") {
		fetch, err := p.parseFetch()
		if err != nil {
			return nil, p.errorf("fetch: %w", err)
		}
		offset.Fetch = fetch
	}

	return offset, nil
}

func (p *state) parseFetch() (ast.Node, error) {
	defer Trace.enter()()

	p.keywords("", "FIRST")
	p.keywords("", "NEXT")

	fetch := ast.Fetch{
		Fetch: p.consumeMatched(),
	}

	count, err := p.parseExpression(0)
	if err != nil {
		return nil, err
	}
	fetch.Count = count

	p.keywords("ROW")
	p.keywords("ROWS")
	fetch.Tail = p.consumeMatched()
	if p.keywords("ONLY") {
		fetch.Tail = append(fetch.Tail, p.consume())
	} else if p.keywords("WITH", "TIES") {
		fetch.Tail = append(fetch.Tail, p.consumeMatched()...)
	}

	return fetch, nil
}
