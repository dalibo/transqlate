package parser_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
)

func TestLimit(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT 1 FROM t LIMIT 1")
	s := n.(ast.Select)
	l := s.Limit.(ast.Limit)
	r.Equal("1", l.Count.(ast.Leaf).Token.Raw)
	r.Nil(l.Offset)

	n = parser.MustParse("SELECT 1 FROM t LIMIT ALL")
	s = n.(ast.Select)
	r.Equal("ALL", s.Limit.(ast.Limit).Count.(ast.Leaf).Token.Raw)
}

func TestLimitOffset(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT 1 FROM t LIMIT 2 OFFSET 3")
	s := n.(ast.Select)
	l := s.Limit.(ast.Limit)
	r.Equal("2", l.Count.(ast.Leaf).Token.Raw)
	r.NotNil(l.Offset)
	offset := l.Offset.(ast.Offset)
	r.Equal("3", offset.Start.(ast.Leaf).Token.Raw)

	n = parser.MustParse("SELECT 1 FROM t LIMIT 2 OFFSET 3 ROWS")
	offset = n.(ast.Select).Limit.(ast.Limit).Offset.(ast.Offset)
	r.Equal("ROWS", offset.Unit.Raw)
}

func TestFetch(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT 1 FROM t FETCH FIRST 2 ROWS ONLY")
	f := n.(ast.Select).Limit.(ast.Fetch)
	r.Equal("2", f.Count.(ast.Leaf).Token.Raw)
	r.Equal("ROWS", f.Tail[0].Raw)

	n = parser.MustParse("SELECT 1 FROM t FETCH NEXT 2 ROW WITH TIES")
	f = n.(ast.Select).Limit.(ast.Fetch)
	r.Equal("2", f.Count.(ast.Leaf).Token.Raw)
	r.Equal("ROW", f.Tail[0].Raw)
	r.Equal("WITH", f.Tail[1].Raw)
	r.Equal("TIES", f.Tail[2].Raw)
}

func TestOffsetFetch(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT 1 FROM t OFFSET 2 ROWS FETCH FIRST 3 ROWS ONLY")
	o := n.(ast.Select).Limit.(ast.Offset)
	r.Equal("2", o.Start.(ast.Leaf).Token.Raw)
	f := o.Fetch.(ast.Fetch)
	r.Equal("3", f.Count.(ast.Leaf).Token.Raw)
}
