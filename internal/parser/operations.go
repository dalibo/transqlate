package parser

import (
	"gitlab.com/dalibo/transqlate/ast"
)

var betweenPrec = 26

// Type of a function that parses an operation.
// parseExpression uses this type to parse operations.
type parseOpFunc func(left ast.Node, precedence int) (ast.Node, error)

// lookaheadOperator returns the precedence of the next token and how to parse it.
//
// Returns 0 if the next token is another expression.
// This is used to implement the Pratt algorithm.
// See https://matklad.github.io/2020/04/13/simple-but-powerful-pratt-parsing.html
// for more information.
//
// The precedence is used to determine the order of operations. The higher the
// precedence, the earlier the operation is performed.
// e.g. 1 + 2 * 3 is parsed as 1 + (2 * 3) because * has higher precedence than +.
//
// The parseOpFunc is a function that takes the left expression and the precedence
// of the current operator and returns the parsed node.
//
// Something important to know: when tokens are matched, the position of the farthest matched token
// is kept. So that consumeAll() or consume() can be used to consume the whole token sequence
// without repetition. parseInfix assume all the tokens of the operator are matched. Thus, to add a
// new infix operator, just add the operator to the switch statement.
func (p *state) lookaheadOperator() (parseOpFunc, int) {
	switch {
	case p.operators("."):
		return p.parseInfix, 60
	case p.operators("::"):
		return p.parseInfix, 55
	case p.operators("(+)"):
		return p.parsePostfix, 50
	case p.punctuations("(", "["):
		return func(expr ast.Node, _ int) (ast.Node, error) {
			return p.parseCall(expr)
		}, 50
		// Prefix operators are 50: NOT, EXISTS, PRIOR, +, -
	case p.punctuations("*"), p.operators("/", "%"), p.keywords("DIV"), p.keywords("MOD"):
		return p.parseInfix, 40
	case p.operators("||"):
		return p.parseConcat, 35
	case p.operators("+", "-"):
		return p.parseInfix, 30
	case p.operators(".."):
		return p.parseInfix, 29
	case p.keywords("BETWEEN"), p.keywords("NOT", "BETWEEN"):
		return func(expr ast.Node, _ int) (ast.Node, error) {
			return p.parseBetween(expr)
		}, betweenPrec
	case p.keywords("IN"), p.keywords("NOT", "IN"):
		return p.parseInfix, 26
	case p.keywords("REGEXP"):
		return p.parseInfix, 25
	case p.keywords("LIKE"), p.keywords("NOT", "LIKE"):
		return p.parseInfix, 25
	case p.keywords("IS"):
		return p.parseInfix, 24
	case p.operators("<", "=<", "!=", "=", "==", ">=", ">", "<>", "<=>", "~"):
		return p.parseInfix, 20
	case p.keywords("AND"):
		return p.parseInfix, 10
	case p.keywords("OR"):
		return p.parseInfix, 5
	case p.keywords("OVER"):
		return p.parseInfix, 3
	}
	return nil, 0
}

// parsePrefix parses a prefixed operation.
func (p *state) parsePrefix(precedence int) (ast.Node, error) {
	defer Trace.enter()()

	n := ast.Prefix{
		Token: p.consume(),
	}
	expr, err := p.parseExpression(precedence)
	if err != nil {
		return nil, err
	}
	n.Expression = expr
	return n, nil
}

// parseInfix parses an infix operation with a single token operator.
func (p *state) parseInfix(left ast.Node, precedence int) (ast.Node, error) {
	defer Trace.enterf("precedence=%d", precedence)()

	n := ast.Infix{
		Left: left,
		Op:   p.consumeMatched(),
	}
	right, err := p.parseExpression(precedence)
	if err != nil {
		return nil, err
	}
	n.Right = right
	return n, nil
}

// parsePostfix parses an operation with operator after operand
// e.g. t.id(+)
func (p *state) parsePostfix(operand ast.Node, _ int) (ast.Node, error) {
	return ast.Postfix{
		Expression: operand,
		Token:      p.consume(),
	}, nil
}

func (p *state) parseConcat(left ast.Node, _ int) (ast.Node, error) {
	defer Trace.enter()()

	group := ast.Array{}
	group.Items = append(group.Items, ast.Postfix{
		Expression: left,
		Token:      p.consume(),
	})

	right, err := p.parseExpression(0)
	if err != nil {
		return nil, err
	}
	tail, ok := right.(ast.Array)
	if ok && tail.Items[0].Is("||") {
		group.Items = append(group.Items, tail.Items...)
	} else {
		group.Items = append(group.Items, ast.Postfix{Expression: right})
	}
	return group, nil
}
