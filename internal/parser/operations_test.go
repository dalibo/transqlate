package parser_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/internal/parser"
)

func TestCompositeIdentifier(t *testing.T) {
	r := require.New(t)
	n, err := parser.Parse(lexer.New("<stdin>", "nsp.t"))
	r.NoError(err)
	i := n.(ast.Infix)
	r.Equal(".", i.Op[0].Raw)
}

func TestIsNull(t *testing.T) {
	r := require.New(t)
	n, err := parser.Parse(lexer.New("", "1 IS NULL"))
	r.NoError(err)
	i := n.(ast.Infix)
	r.Equal("IS", i.Op[0].Raw)
	r.Equal("NULL", i.Right.(ast.Leaf).Token.Raw)
}

func TestNotLike(t *testing.T) {
	r := require.New(t)
	n, err := parser.Parse(lexer.New("", "1 NOT LIKE 2"))
	r.NoError(err)
	notLike := n.(ast.Infix)
	r.Equal("NOT", notLike.Op[0].Raw)
	r.Equal("LIKE", notLike.Op[1].Raw)
}

func TestNot(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("NOT NULL")
	operation := n.(ast.Prefix)
	r.Equal("NOT", operation.Token.Str)
	r.Equal("NULL", operation.Expression.(ast.Leaf).Token.Str)
}

func TestJoinMark(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("tie.fighter ( + )")
	operation := n.(ast.Postfix)
	r.Equal("(+)", operation.Token.Str)
}

func TestPrior(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("prior col = col")
	equal := n.(ast.Infix)
	prior := equal.Left.(ast.Prefix)
	r.True(prior.Is("PRIOR"))
}
