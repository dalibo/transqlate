package parser

import (
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// parseOrderBy parses an ORDER BY clause.
func (p *state) parseOrderBy() (ast.Node, error) {
	defer Trace.enter()()

	orderby := ast.OrderBy{
		OrderBy: p.consumeMatched(),
	}

	for {
		criteria, err := p.parseOrderCriterium()
		if err != nil {
			return nil, err
		}
		item := ast.Postfix{Expression: criteria}

		if p.punctuations(",") {
			item.Token = p.consume()
		}
		orderby.Criteria = append(orderby.Criteria, item)
		if item.Token.IsZero() {
			break
		}
	}

	return orderby, nil
}

// parseOrderCriterium parses an ORDER BY criteria.
func (p *state) parseOrderCriterium() (ast.Node, error) {
	expr, err := p.parseExpression(0)
	if err != nil {
		return nil, err
	}
	criteria := ast.OrderCriterium{
		Expression: expr,
	}
	if p.keywords("ASC") || p.keywords("DESC") {
		criteria.Tail = p.consumeMatched()
	} else if p.keywords("USING") {
		criteria.Tail = p.consumeMatched()
		if !p.seek(lexer.Operator) {
			return nil, p.unexpected()
		}
		criteria.Tail = append(criteria.Tail, p.consume())
	}

	if p.keywords("NULLS") {
		criteria.Tail = append(criteria.Tail, p.consume())
		if p.keywords("FIRST") || p.keywords("LAST") {
			criteria.Tail = append(criteria.Tail, p.consume())
		} else {
			return nil, p.unexpected()
		}
	}
	return criteria, nil
}
