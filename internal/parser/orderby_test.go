package parser_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestOrderBySingle(t *testing.T) {
	r := require.New(t)
	sql := "SELECT col FROM t ORDER BY 1"
	n := parser.MustParse(sql)
	o := n.(ast.Select).OrderBy.(ast.OrderBy)
	r.Len(o.Criteria, 1)
	r.Equal(sql, lexer.Write(n))
}

func TestOrderByComposite(t *testing.T) {
	r := require.New(t)
	sql := dedent.Dedent(`
	SELECT col
	FROM t
	ORDER SIBLINGS BY
	1 ASC,
	2 NULLS FIRST,
	3 USING <,
	4 DESC NULLS LAST;
	`)
	n := parser.MustParse(sql)
	o := n.(ast.Statement).Expression.(ast.Select).OrderBy.(ast.OrderBy)
	r.Len(o.Criteria, 4)
	r.Equal(sql, lexer.Write(n))
}
