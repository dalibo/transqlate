package parser

import (
	"fmt"
	"slices"
	"strings"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// Parse parses the input and returns a tree of nodes.
func Parse(l *lexer.Lexer) (ast.Node, error) {
	return newParser(l).parse()
}

// MustParse return an AST or panics.
func MustParse(sql string, args ...any) ast.Node {
	if len(args) > 0 {
		sql = fmt.Sprintf(sql, args...)
	}
	p := newParser(lexer.New("<builtin>", sql))
	n, err := p.parse()
	if err != nil {
		panic(err)
	}
	return n
}

type state struct {
	input   *lexer.Lexer
	tokens  lexer.Tokens
	pos     int // position of next unprocessed token.
	matched []lexer.Token
}

func newParser(input *lexer.Lexer) *state {
	return &state{
		input: input,
	}
}

// parse parses the input and returns the root of the AST.
func (p *state) parse() (ast.Node, error) {
	for token := p.input.Next(); ; token = p.input.Next() {
		if token.Type == lexer.Error {
			return nil, token.Error
		}
		token.Normalize()

		p.tokens = append(p.tokens, token)

		if token.Type == lexer.EOF {
			break
		}
	}
	return p.parseSnippet()
}

// errorf returns an error at the current position.
func (p *state) errorf(format string, args ...interface{}) error {
	// Emit a trace event only if the error is not wrapping another error.
	var isWrapping bool
	for _, arg := range args {
		_, isWrapping = arg.(error)
		if isWrapping {
			break
		}
	}
	if !isWrapping {
		t := p.tokens[p.pos]
		fn := callerFunction(1)
		Trace.logf("error: %s: %s %q", fn, t.Type, t.Raw)
	}
	p.tokens[p.pos].Error = fmt.Errorf(format, args...)

	return lexer.Err{
		Source:     p.input.Source,
		Line:       p.tokens[p.pos].Line,
		Column:     p.tokens[p.pos].Column,
		LineTokens: p.tokens.LineAt(p.pos),
		Err:        p.tokens[p.pos].Error,
	}
}

func (p *state) unexpected() error {
	return p.errorf("unexpected %s: %s", p.tokens[p.pos].Type, p.tokens[p.pos])
}

// consume step the next token and returns it.
func (p *state) consume() lexer.Token {
	t := p.tokens[p.pos]
	// Prefer matched tokens for identifier as keyword case. The matched token is transtyped to a
	// keyword token.
	if len(p.matched) > 0 {
		t = p.matched[0]
		p.matched = p.matched[1:]
	}
	p.pos++
	Trace.logf("consume: %s %q", t.Type, t.Raw)
	return t
}

func (p *state) consumeMatched() (out []lexer.Token) {
	if len(p.matched) == 0 {
		panic("no token matched")
	}
	out = p.matched
	p.matched = nil
	for _, t := range out {
		Trace.logf("consume: %s %q", t.Type, t.Raw)
		p.pos++
	}
	return
}

// seek returns true if the next token matches the given type and value. If value is empty, only the
// type is checked. Stores the matched token in p.matched. Resets matched tokens. Use seekAhead to
// match several tokens with multiple calls.
func (p *state) seek(tt lexer.TokenType, v ...string) bool {
	if p.tokens[p.pos].Type != tt {
		return false
	}
	if len(v) == 0 || v[0] == "" || slices.Contains(v, p.tokens[p.pos].Str) {
		p.matched = []lexer.Token{p.tokens[p.pos]}
		return true
	}
	return false
}

// seekAhead matches a token after the current matched one.
//
// Preserves the previous matched tokens.
// Use consumeMatched() to get all matched tokens.
func (p *state) seekAhead(tt lexer.TokenType, v ...string) bool {
	t := p.tokens[p.pos+len(p.matched)]
	if t.Type != tt {
		return false
	}
	if len(v) == 0 || v[0] == "" || slices.Contains(v, t.Str) {
		p.matched = append(p.matched, t)
		return true
	}
	return false
}

// keywords returns true if the next tokens matches the given keywords in order.
// Does not consume the tokens.
// keyword is case insensitive.
func (p *state) keywords(v ...string) bool {
	var matched []lexer.Token
	for i, k := range v {
		t := p.tokens[p.pos+i]
		if k != "" && t.Type == lexer.Identifier {
			if t.Quoted() { // Refuse quoted identifiers.
				return false
			}
			// Translate token to keyword.
			t.Type = lexer.Keyword
			t.Normalize()
		}

		if t.Type != lexer.Keyword {
			return false
		}

		// Uppercase for identifier case.
		TOKEN := strings.ToUpper(t.Str)
		if k != "" && k != TOKEN {
			return false
		}
		matched = append(matched, t)
	}
	p.matched = matched
	return true
}

// operators returns true if the next token is a punctuation within the given value.
func (p *state) operators(v ...string) bool {
	return p.seek(lexer.Operator, v...)
}

// punctuations returns true if the next token is a punctuation within the given value.
func (p *state) punctuations(v ...string) bool {
	return p.seek(lexer.Punctuation, v...)
}
