package parser

import (
	"fmt"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// parseSelect parses a SELECT statement.
func (p *state) parseSelect() (ast.Node, error) {
	defer Trace.enter()()

	var err error
	var select_ ast.Select

	if p.keywords("WITH") {
		select_.With, err = p.parseWith()
		if err != nil {
			return nil, p.errorf("with: %w", err)
		}
	}

	if !p.keywords("SELECT") {
		return nil, p.unexpected()
	}

	p.keywords("", "ALL")
	select_.Select = p.consumeMatched()

	if p.keywords("DISTINCT") {
		select_.Distinct, err = p.parseDistinct()
		if err != nil {
			return nil, err
		}
	}

	var expr ast.Node
	for {
		expr, err = p.parseAliasedExpression()
		if err != nil {
			return select_, err
		}

		item := ast.Postfix{Expression: expr}
		if p.punctuations(",") {
			item.Token = p.consume()
		}
		select_.List = append(select_.List, item)
		if item.Token.IsZero() {
			break // No comma, break.
		}
	}

	if p.keywords("FROM") {
		select_.From, err = p.parseFrom()
		if err != nil {
			return nil, p.errorf("from: %w", err)
		}
	}
	if p.keywords("WHERE") {
		select_.Where, err = p.parseWhere()
		if err != nil {
			return nil, p.errorf("where: %w", err)
		}
	}
	if p.keywords("START", "WITH") {
		select_.Hierarchy, err = p.parseStartWith()
		if err != nil {
			return nil, p.errorf("start with: %w", err)
		}
	}
	if p.keywords("CONNECT", "BY") {
		select_.Hierarchy, err = p.parseConnectBy()
		if err != nil {
			return nil, p.errorf("connect by: %w", err)
		}
	}

	// Accept Oracle HAVING before GROUP BY
	if p.keywords("HAVING") {
		select_.GroupBy, err = p.parseHaving()
		if err != nil {
			return nil, p.errorf("having: %w", err)
		}
	}

	if p.keywords("GROUP", "BY") {
		select_.GroupBy, err = p.parseGroupBy()
		if err != nil {
			return nil, p.errorf("group by: %w", err)
		}
	}

	if p.keywords("ORDER", "BY") || p.keywords("ORDER", "SIBLINGS", "BY") {
		select_.OrderBy, err = p.parseOrderBy()
		if err != nil {
			return nil, p.errorf("order by: %w", err)
		}
	}

	if p.keywords("LIMIT") {
		select_.Limit, err = p.parseLimit()
		if err != nil {
			return nil, p.errorf("limit: %w", err)
		}
	} else if p.keywords("OFFSET") {
		select_.Limit, err = p.parseOffset()
		if err != nil {
			return nil, p.errorf("offset: %w", err)
		}
	} else if p.keywords("FETCH") { // PostgreSQL accepts FETCH before OFFSET.
		select_.Limit, err = p.parseFetch()
		if err != nil {
			return nil, p.errorf("fetch: %w", err)
		}
	}

	if p.keywords("MINUS") || p.keywords("EXCEPT") || p.keywords("UNION") || p.keywords("INTERSECT") {
		// Match ALL or DISTINCT after set operator.
		p.keywords("", "ALL")
		p.keywords("", "DISTINCT")
		return p.parseInfix(select_, 0)
	}

	return select_, nil
}

func (p *state) parseDistinct() (d ast.Clause, err error) {
	defer Trace.enter()()
	if !p.keywords("DISTINCT", "ON") {
		d.Keywords = p.consumeMatched()
		return
	}
	d.Keywords = p.consumeMatched() // Consume also ON
	// Use an array for column list.
	d.Expression, err = p.parseArray()
	return
}

// parseAliasedExpression
//
// Accept explicit AS alias or simple identifier.
// Use it in SELECT list.
func (p *state) parseAliasedExpression() (expr ast.Node, err error) {
	defer Trace.enter()()
	expr, err = p.parseExpression(0)
	if err != nil {
		return
	}
	if p.keywords("AS") || p.seek(lexer.Identifier) || p.seek(lexer.String) {
		expr, err = p.parseAlias(expr, 0)
	}
	return
}

// parseFrom parses a FROM clause.
//
// PostgreSQL: https://www.postgresql.org/docs/current/sql-select.html
// Oracle: https://docs.oracle.com/en/database/oracle/oracle-database/23/sqlrf/SELECT.html
// SQLite: https://www.sqlite.org/lang_select.html
// MySQL: https://dev.mysql.com/doc/refman/8.0/en/select.html
// SQL Server: https://learn.microsoft.com/en-us/sql/t-sql/queries/from-transact-sql?view=sql-server-ver16
func (p *state) parseFrom() (ast.From, error) {
	defer Trace.enter()()

	from := ast.From{
		From: p.consume(),
	}
	var err error
	from.Tables, err = p.parseFromTables()
	if err != nil {
		return from, err
	}
	return from, err
}

func (p *state) parseFromTables() (ast.Items, error) {
	defer Trace.enter()()

	var tables ast.Items
	for {
		expr, err := p.parseTableExpression()
		if err != nil {
			return tables, err
		}

		// Search for JOIN operators.
		for p.keywords("JOIN") || p.keywords("NATURAL") || p.keywords("", "JOIN") || p.keywords("", "", "JOIN") {
			expr, err = p.parseJoin(expr)
			if err != nil {
				return nil, err
			}
		}

		item := ast.Postfix{Expression: expr}
		if p.punctuations(",") {
			item.Token = p.consume()
		}
		tables = append(tables, item)
		if item.Token.IsZero() {
			break // No comma, break.
		}
	}
	return tables, nil
}

func (p *state) parseTableExpression() (ast.Node, error) {
	defer Trace.enter()()

	var expr ast.Node
	var err error
	switch {
	case p.punctuations("("):
		// Subselect case: (SELECT ...)
		if p.seekAhead(lexer.Keyword, "SELECT") {
			expr, err = p.parseExpression(0) // Err checked below.
		} else {
			// Wrapped tables case: (table1, table2 AS t)
			array := ast.Array{
				Open: p.consume(),
			}
			array.Items, err = p.parseFromTables()
			if err != nil {
				return nil, err
			}
			if !p.punctuations(")") {
				return nil, p.unexpected()
			}
			array.Close = p.consume()
			expr = array
		}
	case p.keywords("ONLY"), p.keywords("TABLE"): // case Oracle Table function
		expr, err = p.parseExpression(0) // Err checked below.
	case p.seek(lexer.Identifier):
		// Case table name, qualified or not.
		expr, err = p.parseExpression(0) // Err checked below.
	default:
		return nil, p.unexpected()
	}

	if err != nil {
		return nil, err
	}

	// Parse alias for either subquery or table.
	if p.keywords("AS") || p.seek(lexer.Identifier) {
		expr, err = p.parseAlias(expr, 0)
		if err != nil {
			return nil, err
		}
	}

	return expr, nil
}

// parseJoin parses a JOIN expression
//
// Assume JOIN keywords are matched.
func (p *state) parseJoin(left ast.Node) (ast.Node, error) {
	defer Trace.enter()()

	join := ast.Join{
		Left: left,
		Join: p.consumeMatched(),
	}
	if join.Join[0].Str == "NATURAL" {
		if !p.keywords("JOIN") && !p.keywords("", "JOIN") && !p.keywords("", "", "JOIN") {
			return nil, p.unexpected()
		}
		join.Join = append(join.Join, p.consumeMatched()...)
	}
	right, err := p.parseTableExpression()
	if err != nil {
		return nil, fmt.Errorf("join: %w", err)
	}
	join.Right = right

	var condition ast.Node
	if p.keywords("ON") {
		condition, err = p.parseWhere()
		if err != nil {
			return nil, fmt.Errorf("on: %w", err)
		}

	} else if p.keywords("USING") {
		using := p.consume()
		expr, err := p.parseArray()
		if err != nil {
			return nil, fmt.Errorf("using: %w", err)
		}
		condition = ast.Prefix{
			Token:      using,
			Expression: expr,
		}
	}
	join.Condition = condition

	return join, nil
}

// parseWhere parses a WHERE clause.
func (p *state) parseWhere() (ast.Where, error) {
	defer Trace.enter()()

	where := ast.Where{
		Keyword: p.consume(), // Assume WHERE or ON is matched
	}

	c, err := p.parseExpression(0)
	if err != nil {
		return ast.Where{}, err
	}
	// Split ANDs as a list of Prefix <AND right>. The tip of the tree is the *last* AND.
	i, _ := c.(ast.Infix)
	for i.Is("AND") {
		oneSideAnd := ast.Prefix{Token: i.Op[0], Expression: i.Right}
		where.Conditions = append([]ast.Prefix{oneSideAnd}, where.Conditions...)
		c = i.Left // Chain backward.
		i, _ = i.Left.(ast.Infix)
	}
	// Add the first non-AND condition.
	where.Conditions = append([]ast.Prefix{{Expression: c}}, where.Conditions...)
	return where, nil
}
