package parser_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/internal/parser"
)

func TestWith(t *testing.T) {
	r := require.New(t)
	sql := dedent.Dedent(`
	WITH RECURSIVE cte0 AS (
		SELECT 1 UNION SELECT 2
	),
	cte1 AS NOT MATERIALIZED (
		SELECT 3
	),
	cte2(col0, col1) AS MATERIALIZED (
		WITH cte3 AS (SELECT 4)
		SELECT 5
	)
	SELECT * FROM cte0
	`)
	n := parser.MustParse(sql)
	r.Equal(sql, lexer.Write(n))
	s := n.(ast.Select)
	r.False(s.With.IsZero())
	r.False(s.With.Recursive.IsZero())
	r.Len(s.With.CTEs, 3)
}

func TestSearchCycle(t *testing.T) {
	r := require.New(t)
	sql := dedent.Dedent(`
	WITH cte_search_cycle AS (SELECT 0)
	SEARCH BREADTH FIRST BY col0 SET colorder
	CYCLE col1, col2 SET is_cycle USING col3,
	cte_search AS (SELECT 1)
	SEARCH BREADTH FIRST BY col4 SET colorder,
	cte_cycle AS (SELECT 2)
	CYCLE col5, col6 SET is_cycle TO 'value' DEFAULT 'default' USING col7
	SELECT * FROM cte_search_cycle, cte_search, cte_cycle
	`)
	n := parser.MustParse(sql)
	r.Equal(sql, lexer.Write(n))
	s := n.(ast.Select)
	r.False(s.With.IsZero())
	r.True(s.With.Recursive.IsZero())
	r.Len(s.With.CTEs, 3)

	cte := s.With.CTEs[0]
	r.Equal("cte_search_cycle", cte.Name.Str)
	r.NotNil(cte.Search)
	r.NotNil(cte.Cycle)

	cte = s.With.CTEs[1]
	r.Equal("cte_search", cte.Name.Str)
	r.NotNil(cte.Search)
	r.Nil(cte.Cycle)

	cte = s.With.CTEs[2]
	r.Equal("cte_cycle", cte.Name.Str)
	r.Nil(cte.Search)
	r.NotNil(cte.Cycle)
}

func TestIndentWith(t *testing.T) {
	r := require.New(t)
	sql := dedent.Dedent(`
	WITH
	cte0 AS (SELECT 1
	UNION
	SELECT 2),
	cte1 AS (SELECT 2)
	SELECT * FROM cte0, cte1
	`)[1:]
	n := ast.Indent(parser.MustParse(sql), "")
	r.Equal(dedent.Dedent(`
	WITH cte0 AS (
	    SELECT 1

	     UNION

	    SELECT 2
	),
	cte1 AS (
	    SELECT 2
	)
	SELECT * FROM cte0, cte1
	`)[1:], lexer.Write(n))
}
