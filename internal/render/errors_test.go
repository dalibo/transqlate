package render_test

import (
	"errors"
	"fmt"
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/render"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/rewrite"
)

func TestCommentSimpleError(t *testing.T) {
	r := require.New(t)
	tokens := lexer.MustLex("\nSELECT * FROM t1")
	tokens[1].Error = fmt.Errorf("Simple error")
	tokens = render.CommentErrors(tokens)
	r.Equal(dedent.Dedent(`
	-- Simple error
	SELECT * FROM t1`), lexer.Write(tokens))
}

func TestCommentRuleError(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("\nSELECT * FROM t1")
	tokens := append(lexer.Tokens{}, n.Tokens()...)
	tokens[0].Error = rewrite.Err{
		Err:  fmt.Errorf("Rule error"),
		Node: n,
		Rule: "my rule",
	}
	tokens = render.CommentErrors(tokens)
	r.Equal(dedent.Dedent(`
	-- TRANSLATION ERROR at +2:1: Rule error rule="my rule"
	SELECT * FROM t1`), lexer.Write(tokens))
}

func TestCommentMultipleErrors(t *testing.T) {
	r := require.New(t)
	tokens := lexer.MustLex("\nSELECT *\nFROM t1")
	tokens[2].Error = fmt.Errorf("First error")
	tokens[3].Error = fmt.Errorf("Second error")
	tokens = render.CommentErrors(tokens)
	r.Equal(dedent.Dedent(`
	SELECT *
	-- First error
	-- Second error
	FROM t1`), lexer.Write(tokens))
}

func TestHintError(t *testing.T) {
	r := require.New(t)
	n := parser.MustParse("SELECT *\nFROM t\nWHERE 1 = 1")
	tokens := render.Renumber(lexer.Token{}, n)
	tokens[2].Error = errors.Join(rewrite.Err{
		Err:  fmt.Errorf("toto"),
		Node: n.(ast.Select).From,
		Rule: "my error",
	})
	r.Len(render.Renumber(lexer.Token{}, n.(ast.Select).From), 2)

	hinter := render.NewErrorHinter(tokens)
	tk, inError := hinter.Next()
	r.Equal("SELECT", tk.Raw)
	r.False(inError)
	tk, inError = hinter.Next()
	r.Equal("*", tk.Raw)
	r.False(inError)
	tk, inError = hinter.Next()
	r.Equal("FROM", tk.Raw)
	r.True(inError)
	tk, inError = hinter.Next()
	r.Equal("t", tk.Raw)
	r.True(inError)
	tk, inError = hinter.Next()
	r.Equal("WHERE", tk.Raw)
	r.False(inError)
}

func TestWriteCarret(t *testing.T) {
	r := require.New(t)
	s := render.Carret("\n\t--\nSELECT\t\t-", 8, 1)
	r.Equal(`
    --
SELECT        -
              ^`, s)
}
