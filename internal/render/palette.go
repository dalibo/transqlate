package render

import "github.com/mgutz/ansi"

type colorFunc func(string) string

var (
	colorComment     colorFunc
	colorConstant    colorFunc
	colorError       colorFunc
	colorIdentifier  colorFunc
	colorIndentation colorFunc
	colorKeyword     colorFunc
	colorString      colorFunc
	colorType        colorFunc
)

func init() {
	colorComment = ansi.ColorFunc("default+d")
	colorConstant = ansi.ColorFunc("magenta")
	colorError = ansi.ColorFunc("red+bu")
	colorIdentifier = ansi.ColorFunc("cyan")
	colorIndentation = ansi.ColorFunc("blue+d")
	colorKeyword = ansi.ColorFunc("red+h")
	colorString = ansi.ColorFunc("green")
	colorType = ansi.ColorFunc("yellow")
}
