package rules

import (
	"fmt"

	"gitlab.com/dalibo/transqlate/ast"
)

type RemoveLastArgument struct {
	Function string
	Position int // 0-based position of argument in call args list
}

func (r RemoveLastArgument) String() string {
	return fmt.Sprintf("remove last argument from %s", r.Function)
}

func (r RemoveLastArgument) Match(n ast.Node) bool {
	// Check function name.
	// We don't support schema yet because such rules usually applies to builtin functions.
	c, _ := n.(ast.Call)
	if !c.Is(r.Function) {
		return false
	}

	// Check args.
	return len(c.Args) == r.Position+1
}

func (r RemoveLastArgument) Rewrite(n ast.Node) (ast.Node, error) {
	c := n.(ast.Call)
	c.Args.Delete(r.Position)
	return c, nil
}

type AddMissingArgument struct {
	Function string
	Position int // 0-based position of argument in call args list
	Value    ast.Node
}

func (r AddMissingArgument) String() string {
	return fmt.Sprintf("Add missing argument #%d to %s()", r.Position, r.Function)
}

func (r AddMissingArgument) Match(n ast.Node) bool {
	// Check function name.
	c, _ := n.(ast.Call)
	if !c.Is(r.Function) {
		return false
	}

	// Match call args len of 2 to add argument at position 2.
	return len(c.Args) == r.Position
}

func (r AddMissingArgument) Rewrite(n ast.Node) (ast.Node, error) {
	c := n.(ast.Call)
	c.Args.Append(r.Value)
	return c, nil
}
