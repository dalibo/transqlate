package rules_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/internal/rules"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestRemoveLastArg(t *testing.T) {
	r := require.New(t)
	comma := lexer.Token{Raw: ","}
	comma.Normalize()
	n := parser.MustParse("foo(1,2,3)")

	rule := rules.RemoveLastArgument{Function: "foo", Position: 3}
	r.False(rule.Match(n))
	rule = rules.RemoveLastArgument{Function: "foo", Position: 2}
	r.True(rule.Match(n))
	n, err := rule.Rewrite(n)
	r.NoError(err)
	call := n.(ast.Call)
	r.Equal(2, len(call.Args))
}
