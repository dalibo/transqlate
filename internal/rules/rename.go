package rules

import (
	"fmt"
	"strings"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

type RenameConstant struct {
	From, To string
}

func (r RenameConstant) String() string {
	return fmt.Sprintf("%s -> %s", r.From, r.To)
}

func (r RenameConstant) Match(n ast.Node) bool {
	k, _ := n.(ast.Leaf)
	if k.Token.Type != lexer.Identifier {
		return false
	}
	return k.In(r.From)
}

func (r RenameConstant) Rewrite(n ast.Node) (ast.Node, error) {
	a := n.(ast.Leaf)
	a.Token.Set(lexer.ApplyCase(a.Token.Raw, r.To))
	return a, nil
}

type RenameFunction struct {
	// From must be lower cased as lexer.Token.Normalize() lowers identifier. Depending of
	// lexer.UpperKeywords().
	From, To string
	// Overwrite token type.
	Type lexer.TokenType
}

func (r RenameFunction) String() string {
	return fmt.Sprintf("%s -> %s", r.From, r.To)
}

func (r RenameFunction) Match(n ast.Node) bool {
	c, _ := n.(ast.Call)
	return c.Is(r.From)
}

func (r RenameFunction) Rewrite(n ast.Node) (ast.Node, error) {
	c := n.(ast.Call)
	f := c.Function.(ast.Leaf)
	if r.Type != lexer.Void {
		f.Token.Type = r.Type
	}
	f.Token.Raw = r.To
	f.Token.Normalize()
	c.Function = f
	return c, nil
}

type ReplaceOperator struct {
	// From must be UPPER cased as lexer.Token.Normalize() uppers keywords.
	From, To []string
}

func (r ReplaceOperator) String() string {
	return fmt.Sprintf("%s -> %s",
		strings.Join(r.From, " "),
		strings.Join(r.To, " "),
	)
}
func (r ReplaceOperator) Match(n ast.Node) bool {
	i, _ := n.(ast.Infix)
	return i.Is(r.From...)
}

func (r ReplaceOperator) Rewrite(n ast.Node) (ast.Node, error) {
	operation := n.(ast.Infix)
	token0 := operation.Op[0]
	lastToken := operation.Op[len(operation.Op)-1]

	operation.Op = nil
	for _, s := range r.To {
		// Copy first token to initialize.
		t := token0
		// Apply case from first keyword.
		t.Set(lexer.ApplyCase(token0.Raw, s))
		operation.Op = append(operation.Op, t)
	}

	return indentOperator(operation, lastToken), nil
}

func indentOperator(operation ast.Infix, end lexer.Token) ast.Node {
	for i, t := range operation.Op {
		// Ensure space for before following.
		if i > 0 && t.Prefix == "" {
			t.Prefix = " "
		}
		if i == len(operation.Op)-1 {
			// Copy comment after last operator token to new last token.
			t.Suffix = end.Suffix
		} else {
			t.Suffix = ""
		}
		operation.Op[i] = t
	}
	return operation
}
