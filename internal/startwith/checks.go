package startwith

import (
	"fmt"
	"slices"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

func checkInput(node ast.Node) error {
	select_ := node.(ast.Select)
	if !select_.With.IsZero() {
		return fmt.Errorf("WITH clause already present")
	}
	names := select_.ColumnNames()
	if slices.Contains(names, "") {
		return fmt.Errorf("anonymous column")
	}

	names = select_.From.Names()
	if len(names) > 1 {
		return fmt.Errorf("CONNECT BY with composite FROM")
	}
	var err error
	select_.Visit(func(n ast.Node) ast.Node {
		if err != nil {
			return n
		}
		switch n := n.(type) {
		case ast.Call:
			// Assume lexer.UpperIdentifiers is called.
			if n.In("SYS_CONNECT_BY_PATH", "CONNECT_BY_ROOT", "CONNECT_BY_ISLEAF") {
				err = fmt.Errorf("%s in recursive query", lexer.Write(n.Function))
			}
		case ast.ConnectBy:
			lastToken := n.ConnectBy[len(n.ConnectBy)-1]
			if lastToken.Str == "NOCYCLE" {
				err = fmt.Errorf("NOCYCLE in CONNECT BY")
			}
		}
		return n
	})
	if err != nil {
		return err
	}
	return nil
}

func checkOutput(orig ast.Node, out ast.Node) (ast.Node, error) {
	hasPrior := false
	out.Visit(func(n ast.Node) ast.Node {
		prior, _ := n.(ast.Prefix)
		hasPrior = hasPrior || prior.Is("PRIOR")
		return n
	})
	if hasPrior {
		return out, fmt.Errorf("PRIOR operator left")
	}
	return out, nil
}
