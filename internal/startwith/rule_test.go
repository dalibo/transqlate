package startwith_test

import (
	"log/slog"
	"os"
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/oracle"
	"gitlab.com/dalibo/transqlate/rewrite"
)

func TestMain(m *testing.M) {
	slog.SetDefault(slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	})))
	lexer.UpperIdentifiers()
	os.Exit(m.Run())
}

func TestScalar(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", dedent.Dedent(`
	SELECT LEVEL
	FROM employees
	START WITH manager = 1
	CONNECT BY manager = PRIOR id
	`)[1:], oracle.Defaults(), rewrite.Pretty())
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	WITH RECURSIVE hierarchy(level) AS (
	    SELECT 1 AS level
	      FROM employees
	     WHERE manager = 1

	     UNION ALL

	    SELECT "prior".level + 1 AS level
	      FROM employees AS recursion
	      JOIN hierarchy AS "prior"
	        ON recursion.manager = "prior".id
	)
	SELECT level
	  FROM hierarchy AS employees
	`)[1:], sql)
}

func TestSimple(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", dedent.Dedent(`
	SELECT id, name, job, manager
	FROM employees
	START WITH manager = 1 AND name = 'top0'
	CONNECT BY PRIOR id = manager
	ORDER BY id
	`)[1:], oracle.Defaults(), rewrite.Pretty())
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	WITH RECURSIVE hierarchy(id, name, job, manager) AS (
	    SELECT id, name, job, manager
	      FROM employees
	     WHERE manager = 1 AND name = 'top0'

	     UNION ALL

	    SELECT recursion.id, recursion.name, recursion.job, recursion.manager
	      FROM employees AS recursion
	      JOIN hierarchy AS "prior"
	        ON "prior".id = recursion.manager
	)
	SELECT id, name, job, manager
	  FROM hierarchy AS employees
	 ORDER BY id
	`)[1:], sql)
}

func TestLevel(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", dedent.Dedent(`
	SELECT id, name, job, manager, LEVEL
	FROM employees
	START WITH manager = 1 AND name = 'top0'
	CONNECT BY PRIOR id = manager
	ORDER BY id
	`)[1:], oracle.Defaults(), rewrite.Pretty())
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	WITH RECURSIVE hierarchy(id, name, job, manager, level) AS (
	    SELECT id, name, job, manager, 1 AS level
	      FROM employees
	     WHERE manager = 1 AND name = 'top0'

	     UNION ALL

	    SELECT recursion.id, recursion.name, recursion.job, recursion.manager, "prior".level + 1 AS level
	      FROM employees AS recursion
	      JOIN hierarchy AS "prior"
	        ON "prior".id = recursion.manager
	)
	SELECT id, name, job, manager, level
	  FROM hierarchy AS employees
	 ORDER BY id
	`)[1:], sql)
}

func TestComment(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", dedent.Dedent(`
	SELECT id, name, job, manager
	-- we need a recursive join
	FROM employees -- from comment
	START WITH manager = 1 -- start with boss
	CONNECT BY PRIOR id = manager -- each employees has a manager
	ORDER BY id
	`)[1:], oracle.Defaults(), rewrite.Pretty())
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	WITH RECURSIVE hierarchy(id, name, job, manager) AS (
	    SELECT id, name, job, manager
	      -- we need a recursive join
	      FROM employees -- from comment
	     WHERE manager = 1 -- start with boss

	     UNION ALL

	    SELECT recursion.id, recursion.name, recursion.job, recursion.manager
	      FROM employees AS recursion
	      JOIN hierarchy AS "prior"
	        ON "prior".id = recursion.manager -- each employees has a manager
	)
	SELECT id, name, job, manager
	  FROM hierarchy AS employees
	 ORDER BY id
	`)[1:], sql)
}
