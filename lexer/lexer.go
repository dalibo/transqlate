package lexer

import (
	"fmt"
	"strings"
	"unicode/utf8"
)

// MustLex returns tokens of the given SQL string.
//
// Panics on tokenization error.
// Use it to build code from a literal.
func MustLex(sql string, args ...any) (tokens Tokens) {
	if len(args) > 0 {
		sql = fmt.Sprintf(sql, args...)
	}
	l := New("<builtin>", sql)
	for token := l.Next(); ; token = l.Next() {
		if token.Type == Error {
			panic(token.Error)
		}
		token.Normalize()

		if token.Type == EOF {
			break
		}
		tokens = append(tokens, token)
	}
	return
}

type Lexer struct {
	Source     string // Name of the input
	input      string // The input string
	state      stateFn
	pos        int    // Current position in the input
	prefix     int    // Start position of token space and comments beforehand.
	start      int    // Start position of this token
	line       int    // \n count
	col        int    // Current column
	lineTokens Tokens // Tokens of the current line
	tokens     chan Token
}

// New creates a new lexer for the input string.
// The source parameter is used for error reporting.
func New(source, input string) *Lexer {
	return &Lexer{
		Source: source,
		input:  input,
		state:  lexScript,
		tokens: make(chan Token, 3),
	}
}

// Next returns the next token from the input.
//
// Last token is EOF. Iterating after EOF panics.
func (l *Lexer) Next() Token {
	for {
		select {
		case token := <-l.tokens:
			return token
		default:
			if l.state == nil {
				panic("lexer state is nil")
			}
			l.state = l.state(l)
		}
	}
}

// emit passes a token back to the client.
func (l *Lexer) emit(tt TokenType) {
	if tt != EOF && l.start == l.pos {
		panic(fmt.Sprintf("trying to emit empty at +%d:%d", 1+l.line, 1+l.col))
	}
	t := Token{
		Prefix: l.input[l.prefix:l.start],
		Raw:    l.input[l.start:l.pos],
		Type:   tt,
		Line:   l.line,
		Column: l.col,
	}
	l.start = l.pos
	l.col += len(t.Raw)
	t.Suffix = l.suffix()

	l.prefix = l.pos
	l.lineTokens = append(l.lineTokens, t)
	l.tokens <- t
}

// suffix consumes spaces and comment up to new line or EOF, or nil.
func (l *Lexer) suffix() string {
	start := l.pos

	l.acceptMany(" \t") // accept spacing before comment

	if l.accept("\n") || l.peek() == 0 {
		// accept new line or EOF
		l.ignore()
		return l.input[start:l.pos]
	}

	// Handle C-style comments
	if strings.HasPrefix(l.input[l.pos:], "/*") {
		if !l.acceptUntilString("*/") {
			// Backup. Let lexScript handle unterminated comment
			l.pos = start
			return ""
		}
		bak := l.pos
		l.acceptMany(" \t")
		if !l.accept("\n") {
			l.pos = bak // Keep trailing spaces for next token prefix
		}
		l.ignore()
		return l.input[start:l.pos]
	}

	// Handle -- comments
	if strings.HasPrefix(l.input[l.pos:], "--") {
		l.acceptUntilString("\n")
		l.ignore()
		return l.input[start:l.pos]
	}

	// There is a token next. Backup before spaces for next token prefix.
	l.pos = start
	return ""
}

// next returns the next rune in the input.
// Returns 0 if the end of the input was reached.
func (l *Lexer) next() (r rune) {
	if l.pos >= len(l.input) {
		return 0
	}
	r, w := utf8.DecodeRuneInString(l.input[l.pos:])
	l.pos += w
	return r
}

// peek returns but does not consume the next rune in the input.
// Returns 0 if the end of the input was reached.
func (l *Lexer) peek() (r rune) {
	r = l.next()
	if r != 0 {
		l.backup()
	}
	return r
}

// backup steps back one rune. Can only be called once per call of next.
func (l *Lexer) backup() {
	_, w := utf8.DecodeLastRuneInString(l.input[:l.pos])
	l.pos -= w
}

func (l *Lexer) current() rune {
	r, _ := utf8.DecodeLastRuneInString(l.input[:l.pos])
	return r
}

// accept consumes the next rune if it's from the valid set.
func (l *Lexer) accept(valid string) bool {
	r := l.next()
	if r == 0 {
		return false

	}
	if !strings.ContainsRune(valid, r) {
		l.backup()
		return false
	}
	return true
}

// acceptMany consumes a set of runes from the valid set.
func (l *Lexer) acceptMany(valid string) bool {
	found := false
	for {
		r := l.next()
		if r == 0 {
			return found
		}
		if !strings.ContainsRune(valid, r) {
			l.backup()
			return found
		}
		found = true
	}
}

// acceptUntil consumes runes until the stop rune is encountered.
func (l *Lexer) acceptUntil(stop rune) bool {
	for {
		switch r := l.next(); {
		case r == 0:
			return false
		case r == stop:
			l.backup()
			return true
		}
	}
}

// acceptUntil consumes runes until the stop rune is encountered.
//
// consumes stop string.
func (l *Lexer) acceptUntilString(stop string) bool {
	stopPos := strings.Index(l.input[l.pos:], stop)
	if stopPos == -1 {
		return false
	}
	l.pos += stopPos + len(stop)
	return true
}

// ignore skips over the pending input before this point.
func (l *Lexer) ignore() {
	lines := strings.Count(l.input[l.start:l.pos], "\n")
	if lines > 0 {
		l.line += lines
		l.lineTokens = nil
		lastNewline := strings.LastIndex(l.input[:l.pos], "\n")
		l.col = len(l.input[lastNewline+1 : l.pos])
	} else {
		l.col += l.pos - l.start
	}
	l.start = l.pos
}
