package lexer_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/lexer"
)

func TestEmpty(t *testing.T) {
	r := require.New(t)
	l := lexer.New("<stdin>", "")
	r.Equal(lexer.Token{Type: lexer.EOF}, l.Next())
	r.Panics(func() { l.Next() })
}

func TestPrefix(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", " \tSELECT")
	r.Equal(lexer.Token{Prefix: " \t", Raw: "SELECT", Type: lexer.Keyword, Column: 2}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 8}, l.Next())
	r.Panics(func() { l.Next() })

	l = lexer.New("", "-- prefix comment\n SELECT")
	r.Equal(lexer.Token{Prefix: "-- prefix comment\n ", Raw: "SELECT", Type: lexer.Keyword, Line: 1, Column: 1}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1, Column: 7}, l.Next())

	l = lexer.New("", "/** com */\n\n\n")
	r.Equal(lexer.Token{Prefix: "/** com */\n\n\n", Type: lexer.EOF, Line: 3}, l.Next())
}

func TestSuffix(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", "SELECT\n")
	r.Equal(lexer.Token{Raw: "SELECT", Suffix: "\n", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1}, l.Next())

	l = lexer.New("", "SELECT  \n")
	r.Equal(lexer.Token{Raw: "SELECT", Suffix: "  \n", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1}, l.Next())

	l = lexer.New("", "SELECT  -- Comment\n")
	r.Equal(lexer.Token{Raw: "SELECT", Suffix: "  -- Comment\n", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Line: 1}, l.Next())

	l = lexer.New("", "SELECT  /* Multiline\n  comment\n */\n\n")
	r.Equal(lexer.Token{Raw: "SELECT", Suffix: "  /* Multiline\n  comment\n */\n", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Prefix: "\n", Line: 4}, l.Next())

	l = lexer.New("", "SELECT\n\n\n")
	r.Equal(lexer.Token{Raw: "SELECT", Suffix: "\n", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Prefix: "\n\n", Type: lexer.EOF, Line: 3}, l.Next())
}

func TestKeywords(t *testing.T) {
	r := require.New(t)
	l := lexer.New("<stdin>", "SELECT;")
	r.Equal(lexer.Token{Raw: "SELECT", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Raw: ";", Type: lexer.Punctuation, Column: 6}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 7}, l.Next())
}

func TestIdentifier(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", `select sysDate as "identifier";`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "sysDate", Type: lexer.Identifier, Column: 7}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "as", Type: lexer.Keyword, Column: 15}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: `"identifier"`, Type: lexer.Identifier, Column: 18}, l.Next())
	r.Equal(lexer.Token{Raw: ";", Type: lexer.Punctuation, Column: 30}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 31}, l.Next())

	l = lexer.New("", `select sysDate as "iden "" tifier";`)
	r.Equal(lexer.Token{Raw: "select", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "sysDate", Type: lexer.Identifier, Column: 7}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "as", Type: lexer.Keyword, Column: 15}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: `"iden "" tifier"`, Type: lexer.Identifier, Column: 18}, l.Next())
	r.Equal(lexer.Token{Raw: ";", Type: lexer.Punctuation, Column: 34}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 35}, l.Next())

	l = lexer.New("", `TO_DATE('2019-01-01', 'YYYY-MM-DD')`)
	r.Equal(lexer.Token{Raw: "TO_DATE", Type: lexer.Identifier}, l.Next())
	r.Equal(lexer.Token{Raw: "(", Type: lexer.Punctuation, Column: 7}, l.Next())
	r.Equal(lexer.Token{Raw: "'2019-01-01'", Type: lexer.String, Column: 8}, l.Next())
	r.Equal(lexer.Token{Raw: ",", Type: lexer.Punctuation, Column: 20}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "'YYYY-MM-DD'", Type: lexer.String, Column: 22}, l.Next())
	r.Equal(lexer.Token{Raw: ")", Type: lexer.Punctuation, Column: 34}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 35}, l.Next())

	l = lexer.New("", `table table1 table#`)
	r.Equal(lexer.Token{Raw: "table", Type: lexer.Keyword}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "table1", Type: lexer.Identifier, Column: 6}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "table#", Type: lexer.Identifier, Column: 13}, l.Next())

	l = lexer.New("", "`id` `quo``ted`")
	r.Equal(lexer.Token{Raw: "`id`", Type: lexer.Identifier}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "`quo``ted`", Type: lexer.Identifier, Column: 5}, l.Next())
}

func TestNumbers(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", `1 2_000 X'3AB' x'4cd'`)
	r.Equal(lexer.Token{Raw: "1", Type: lexer.Integer}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "2_000", Type: lexer.Integer, Column: 2}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "X'3AB'", Type: lexer.Integer, Column: 8}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "x'4cd'", Type: lexer.Integer, Column: 15}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 21}, l.Next())

	l = lexer.New("", `0.1 .2 3.4E-5 6_000.7`)
	r.Equal(lexer.Token{Raw: "0.1", Type: lexer.Float}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: ".2", Type: lexer.Float, Column: 4}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "3.4E-5", Type: lexer.Float, Column: 7}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "6_000.7", Type: lexer.Float, Column: 14}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 21}, l.Next())

	// corner case: range operator
	l = lexer.New("", `5..6`)
	r.Equal(lexer.Token{Raw: "5", Type: lexer.Integer}, l.Next())
	r.Equal(lexer.Token{Raw: "..", Type: lexer.Operator, Column: 1}, l.Next())
	r.Equal(lexer.Token{Raw: "6", Type: lexer.Integer, Column: 3}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 4}, l.Next())

	l = lexer.New("", `7..a`)
	r.Equal(lexer.Token{Raw: "7", Type: lexer.Integer}, l.Next())
	r.Equal(lexer.Token{Raw: "..", Type: lexer.Operator, Column: 1}, l.Next())
	r.Equal(lexer.Token{Raw: "a", Type: lexer.Identifier, Column: 3}, l.Next())

	// corner case: composite identifier.
	l = lexer.New("", "a.b")
	r.Equal(lexer.Token{Raw: "a", Type: lexer.Identifier}, l.Next())
	r.Equal(lexer.Token{Raw: ".", Type: lexer.Operator, Column: 1}, l.Next())
	r.Equal(lexer.Token{Raw: "b", Type: lexer.Identifier, Column: 2}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 3}, l.Next())
}

func TestOperators(t *testing.T) {
	r := require.New(t)
	l := lexer.New("", `1 + 2`)
	r.Equal(lexer.Token{Raw: "1", Type: lexer.Integer}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "+", Type: lexer.Operator, Column: 2}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "2", Type: lexer.Integer, Column: 4}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 5}, l.Next())

	l = lexer.New("", `3 ! = 4`)
	r.Equal(lexer.Token{Raw: "3", Type: lexer.Integer}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "! =", Type: lexer.Operator, Column: 2}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "4", Type: lexer.Integer, Column: 6}, l.Next())
	r.Equal(lexer.Token{Type: lexer.EOF, Column: 7}, l.Next())

	l = lexer.New("", `( + ) (+)=`)
	r.Equal(lexer.Token{Raw: "( + )", Type: lexer.Operator}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "(+)", Type: lexer.Operator, Column: 6}, l.Next())
	r.Equal(lexer.Token{Prefix: "", Raw: "=", Type: lexer.Operator, Column: 9}, l.Next())

	l = lexer.New("", `a + /* comment */ b * /* comment */ c`)
	r.Equal(lexer.Token{Raw: "a", Type: lexer.Identifier}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "+", Suffix: " /* comment */", Type: lexer.Operator, Column: 2}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "b", Type: lexer.Identifier, Column: 18}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "*", Suffix: " /* comment */", Type: lexer.Punctuation, Column: 20}, l.Next())
	r.Equal(lexer.Token{Prefix: " ", Raw: "c", Type: lexer.Identifier, Column: 36}, l.Next())

	l = lexer.New("", `d..1`)
	r.Equal(lexer.Token{Raw: "d", Type: lexer.Identifier}, l.Next())
	r.Equal(lexer.Token{Raw: "..", Type: lexer.Operator, Column: 1}, l.Next())
	r.Equal(lexer.Token{Raw: "1", Type: lexer.Integer, Column: 3}, l.Next())
}

func TestMustLex(t *testing.T) {
	r := require.New(t)
	tokens := lexer.MustLex("a %s OUTER JOIN b", "LEFT")
	r.Len(tokens, 5)
	r.Equal("a", tokens[0].Str)
	r.Equal("LEFT", tokens[1].Str)
	r.Equal("OUTER", tokens[2].Str)
	r.Equal("JOIN", tokens[3].Str)
	r.Equal("b", tokens[4].Str)

	r.Panics(func() {
		lexer.MustLex("'unterminated string")
	})
}
