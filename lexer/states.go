package lexer

import "strings"

type stateFn func(*Lexer) stateFn

// star (*) is not an operator because of SELECT *.
const operators = "!&+-/:;<=>@|%~"

// lexScript is the top-level state for lexing a script.
func lexScript(l *Lexer) stateFn {
	switch r := l.next(); {
	case r == 0:
		l.emit(EOF)
		return nil
	case r == ' ' || r == '\t' || r == '\n':
		l.ignore()
		return lexScript
	case r == '-':
		n := l.peek()
		if n == '-' {
			l.backup()
			return lexCommentHyphens
		} else {
			return lexOperator
		}
	case r == '/':
		n := l.peek()
		if n == '*' {
			l.backup()
			return lexCommentCStyle
		} else {
			return lexOperator
		}
	case strings.ContainsRune(".0123456789", r):
		l.backup()
		return lexNumber
	case strings.ContainsRune("xX", r):
		l.backup()
		return lexHexNumber
	case r == 'N':
		l.backup()
		return lexUnicode
	case strings.ContainsRune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", r):
		return lexKeywordOrIdentifier
	case r == '\'':
		return lexString
	case r == '$':
		l.backup()
		return lexDollarQuotedString
	case r == '"', r == '`':
		return lexQuotedIdentifier
	case strings.ContainsRune("*,;()[]{}", r):
		l.backup()
		return lexPunctuation
	case strings.ContainsRune(operators, r):
		return lexOperator
	default:
		l.errorf("unrecognized character: %#U", r)
		l.backup()
		return nil
	}
}

// lexCommentCStyle consumes a C-style comment.
func lexCommentCStyle(l *Lexer) stateFn {
	l.acceptUntilString("/*")
	if !l.acceptUntilString("*/") {
		return l.errorf("unterminated comment")
	}
	l.ignore()
	return lexScript
}

// lexCommentHyphens consumes a comment.
func lexCommentHyphens(l *Lexer) stateFn {
	l.acceptUntil('\n')
	l.ignore()
	return lexScript
}

// lexKeywordOrIdentifier consumes a keyword.
func lexKeywordOrIdentifier(l *Lexer) stateFn {
	l.acceptMany("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	if !l.acceptMany("_#$abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") {
		word := strings.ToUpper(l.input[l.start:l.pos])
		if IsKeyword(word) {
			l.emit(Keyword)
			return lexScript
		}
	}

	l.emit(Identifier)
	return lexScript
}

// lexQuotedIdentifier consumes an identifier.
func lexQuotedIdentifier(l *Lexer) stateFn {
	var q rune = l.current()
	for {
		l.acceptUntil(q) // walk until the closing quote
		r := l.next()    // consume the quote
		if r == 0 {
			return l.errorf("unterminated quoted identifier")
		}
		r = l.peek()
		if r != q {
			// Unescaped quote, we're done.
			l.emit(Identifier)
			return lexScript
		}
		// Escaped quote, consume it and continue.
		l.next()
	}
}

// lexString consumes a string literal.
func lexString(l *Lexer) stateFn {
	// lexUnicode may have accepted N
	for {
		l.acceptUntil('\'') // walk until the closing quote
		r := l.next()       // consume the quote
		if r == 0 {
			return l.errorf("unterminated string literal")
		}
		r = l.peek()
		if r != '\'' {
			// Unescaped quote, we're done.
			l.emit(String)
			return lexScript
		}
		// Escaped quote, consume it and continue.
		l.next()
	}
}

// lexDollarQuotedString consumes a tagged string literal.
func lexDollarQuotedString(l *Lexer) stateFn {
	l.next() // consume the $
	l.acceptMany("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_")
	if l.next() != '$' {
		return l.errorf("incomplete string tag")
	}
	tag := l.input[l.start:l.pos]
	if !l.acceptUntilString(tag) {
		return l.errorf("unterminated tagged string literal")
	}
	l.emit(String)
	return lexScript
}

// lexUnicode consumes a Unicode string literal.
func lexUnicode(l *Lexer) stateFn {
	l.accept("N")
	if !l.accept("'") {
		l.backup()
		return lexKeywordOrIdentifier
	}
	return lexString
}

// lexNumber consumes a number or a DOT DOT integer iterator.
func lexNumber(l *Lexer) stateFn {
	hasNumbersBeforePoint := l.acceptMany("0123456789_")
	r := l.peek()
	if r == '.' { // float or integer iterator
		l.next()
		r := l.peek()
		if r == '.' { // DOT DOT iterator
			if hasNumbersBeforePoint {
				l.backup() // first DOT
				l.emit(Integer)
				l.next()
			}
			l.next() // Second DOT
			l.emit(Operator)
			return lexScript
		}
		// Test hasNumbersBeforePoint after to ensure acceptMany is called.
		if !l.acceptMany("0123456789_") && !hasNumbersBeforePoint {
			// A single . without number before and after
			l.emit(Operator)
		} else {
			// A . with either numbers before or after.
			if l.accept("E") {
				// Accept exponent.
				l.accept("+-")
				if !l.acceptMany("0123456789_") {
					return l.errorf("badly formed number")
				}
			}
			l.emit(Float)
		}
		return lexScript
	}
	l.acceptMany("0123456789_")
	l.emit(Integer)
	return lexScript
}

// lexHexNumber consumes a hexadecimal number.
func lexHexNumber(l *Lexer) stateFn {
	l.accept("xX")
	if !l.accept("'") {
		l.backup()
		return lexKeywordOrIdentifier
	}
	l.acceptMany("0123456789abcdefABCDEF")
	l.accept("'")
	l.emit(Integer)
	return lexScript
}

// lexOperator consumes an operator.
func lexOperator(l *Lexer) stateFn {
	l.acceptMany(operators)
	end := l.pos
	l.acceptMany(" \t")
	for !strings.HasPrefix(l.input[l.pos:], "--") && !strings.HasPrefix(l.input[l.pos:], "/*") && l.acceptMany(operators) {
		end = l.pos
		l.acceptMany(" \t")
	}
	l.pos = end // backup before spaces
	l.emit(Operator)
	return lexScript
}

// lexPunctuation consumes a punctuation.
func lexPunctuation(l *Lexer) stateFn {
	bak := l.pos
	if l.accept("(") {
		l.acceptMany(" ")
		if l.accept("+") {
			l.acceptMany(" ")
			if l.accept(")") {
				l.emit(Operator)
				return lexScript
			}
		}
	}

	l.pos = bak
	l.accept("*,;()[]{}")
	l.emit(Punctuation)
	return lexScript
}
