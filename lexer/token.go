package lexer

import (
	"fmt"
	"strconv"
	"strings"
)

// Token holds a element of SQL.
//
// It can be a keyword, an identifier, a string, a number, an operator, a punctuation, etc.
//
// Zero value is a void token. It is ignored when rendering the SQL.
type Token struct {
	Type   TokenType
	Prefix string `dump:"-"` // spaces and comments
	Raw    string // raw value
	Suffix string `dump:"-"` // spaces and comments up to EOL

	// Position in the input string, starting at 0.
	Line   int   `dump:"-"`
	Column int   `dump:"-"`
	Error  error `dump:"-"` // error if any

	// Normalized values
	Str string `dump:"-"` //  e.g. : unquoted string, identifier, uppercased keyword, etc.
	Int int    `dump:"-"` // integer value
}

func (t Token) String() string {
	return t.Raw
}

// IsZero reports whether the token is void.
func (t Token) IsZero() bool {
	return t.Type == Void && t.Raw == ""
}

// Quote reports whether the token is quoted.
//
// It's true for strings and quoted identifiers. Useful to distinguish keyword from identifier in
// some cases.
func (t Token) Quoted() bool {
	if t.Raw == "" {
		return false
	}
	return strings.ContainsAny(t.Raw[:1], `"'`+"`")
}

// BeginsLine reports whether the token is first in line.
func (t Token) BeginsLine() bool {
	if strings.Contains(t.Prefix, "\n") {
		return true
	}
	return len(t.Prefix) == t.Column
}

// EndsLine reports whether the token ends the line.
func (t Token) EndsLine() bool {
	return strings.Contains(t.Suffix, "\n")
}

// Indentation returns the prefix of the token, without comment.
func (t Token) Indentation() string {
	if t.Prefix == " " {
		return ""
	}
	if !strings.Contains(t.Prefix, "\n") {
		return t.Prefix
	}
	return t.Prefix[strings.LastIndex(t.Prefix, "\n")+1:]
}

// Outdentation returns the suffix of the token, without comment.
//
// Usually, it's a new line or empty.
func (t Token) Outdentation() string {
	if strings.Contains(t.Suffix, "--") {
		return t.Suffix[:strings.Index(t.Suffix, "--")] + "\n"
	} else {
		return t.Suffix
	}
}

// LeadingComment returns the comment before the token.
func (t Token) LeadingComment() string {
	if strings.Contains(t.Prefix, "--") {
		return t.Prefix[:strings.LastIndex(t.Prefix, "\n")+1]
	} else if strings.Contains(t.Prefix, "*/") {
		return t.Prefix[:strings.Index(t.Prefix, "*/")+2]
	}
	return ""
}

// TrailingComment returns the comment after the token or emtpy string.
//
// If token only has a trailing new line, empty string is returned.
func (t Token) TrailingComment() string {
	switch {
	case strings.Contains(t.Suffix, "--"):
		return t.Suffix[strings.Index(t.Suffix, "--"):]
	case strings.Contains(t.Suffix, "/*"):
		return t.Suffix[strings.Index(t.Suffix, "/*"):]
	default:
		return ""
	}
}

// Set the raw value of the token.
func (t *Token) Set(raw string) Token {
	t.Raw = raw
	t.Normalize()
	return *t
}

var upperIdentifiers bool

// UpperIdentifiers determine whether to upper case identifiers like Oracle.
//
// It's irreversible.
func UpperIdentifiers() {
	upperIdentifiers = true
}

// Normalize interpret raw value.
func (t *Token) Normalize() {
	switch t.Type {
	case Operator:
		// Remove spaces inside operators.
		s := strings.ReplaceAll(t.Raw, " ", "")
		t.Str = strings.ReplaceAll(s, "\t", "")
	case Keyword:
		// Uppercase keywords.
		t.Str = strings.ToUpper(t.Raw)
	case Identifier:
		r := t.Raw
		if r[0] == '"' || r[0] == '`' {
			q := r[:1]
			qq := fmt.Sprintf("%s%s", q, q)
			// Unquote identifiers.
			r = r[1 : len(r)-1]
			r = strings.ReplaceAll(r, qq, q)
		} else if upperIdentifiers {
			// Uppercase unquoted identifiers, like Oracle.
			r = strings.ToUpper(t.Raw)
		} else {
			// Lowercase unquoted identifiers, like PostgreSQL.
			r = strings.ToLower(t.Raw)
		}
		t.Str = r
	case Integer:
		var err error
		s := strings.ReplaceAll(t.Raw, "_", "")
		base := 10
		if strings.Contains("xX", s[:1]) {
			s = UnquoteString(s[1:])
			base = 16
		}
		v, err := strconv.ParseUint(s, base, 0)
		if err != nil {
			panic(err)
		}
		t.Int = int(v)
	case String:
		// Unquote strings.
		t.Str = UnquoteString(t.Raw)
	default:
		// by default, just use raw value as normalized value
		t.Str = t.Raw
	}
}

type TokenType int

const (
	Void TokenType = iota
	EOF
	Error
	Float
	Identifier
	Integer
	Keyword
	Operator
	Punctuation
	String
	Raw // Raw SQL (not tokenized)
)

func (t TokenType) String() string {
	switch t {
	case EOF:
		return "EOF"
	case Error:
		return "Error"
	case Float:
		return "Float"
	case Identifier:
		return "Identifier"
	case Integer:
		return "Integer"
	case Keyword:
		return "Keyword"
	case Operator:
		return "Operator"
	case Punctuation:
		return "Punctuation"
	case Raw:
		return "Raw"
	case String:
		return "String"
	case Void:
		return "Void"
	default:
		panic("unknown token type")
	}
}
