package mysql

import (
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

type stringAlias struct{}

func (stringAlias) String() string {
	return "string alias"
}

func (stringAlias) Match(n ast.Node) bool {
	alias, _ := n.(ast.Alias)
	return alias.Name.Type == lexer.String
}

func (stringAlias) Rewrite(node ast.Node) (ast.Node, error) {
	alias := node.(ast.Alias)
	alias.Name.Type = lexer.Identifier
	alias.Name.Raw = lexer.QuoteIdentifier(alias.Name.Str)
	alias.Name.Normalize()
	return alias, nil
}
