package mysql_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/mysql"
)

func TestStringAlias(t *testing.T) {
	r := require.New(t)
	sql, err := mysql.Translate("<test>", "SELECT col AS 'alias'")
	r.Nil(err)
	r.Equal(`SELECT col AS "alias"`, sql)

	sql, err = mysql.Translate("<test>", "SELECT col AS 'stupid''alias'")
	r.Nil(err)
	r.Equal(`SELECT col AS "stupid'alias"`, sql)

	sql, err = mysql.Translate("<test>", `SELECT col AS 'anotherstupid"alias'`)
	r.Nil(err)
	r.Equal(`SELECT col AS "anotherstupid""alias"`, sql)
}
