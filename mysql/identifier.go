package mysql

import "gitlab.com/dalibo/transqlate/lexer"

type untick struct{}

func (u untick) String() string {
	return "replace grave accent"
}

func (u untick) Rewrite(t lexer.Token) lexer.Token {
	if t.Type != lexer.Identifier {
		return t
	}
	if t.Raw[0] != '`' {
		return t
	}
	t.Raw = lexer.QuoteIdentifier(t.Str)
	t.Normalize()
	return t
}
