package mysql_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/mysql"
)

func TestBackTick(t *testing.T) {
	r := require.New(t)
	sql, err := mysql.Translate("<test>", "SELECT `col`")
	r.Nil(err)
	r.Equal(`SELECT "col"`, sql)

	sql, err = mysql.Translate("<test>", "SELECT `col``tick`")
	r.Nil(err)
	r.Equal(`SELECT "col`+"`"+`tick"`, sql)
}
