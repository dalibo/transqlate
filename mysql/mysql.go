package mysql

import (
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/internal/rules"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/rewrite"
)

func Engine() rewrite.Engine {
	engine := rewrite.New("mysql", rewrite.Parser(parse))

	engine.Append(
		// node rules
		rules.RenameFunction{From: "ifnull", To: "COALESCE", Type: lexer.Keyword},
		stringAlias{},
		//token rules
		untick{},
	)

	return engine
}

func parse(source, input string) (ast.Node, error) {
	return parser.Parse(lexer.New(source, input))
}

// Translate a SQL query from MySQL to PostgreSQL.
func Translate(source, sql string) (string, error) {
	e := Engine()
	tokens, err := e.Translate(source, sql)
	sql = lexer.Write(tokens)
	return sql, err
}
