package mysql_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/lexer"
	"gitlab.com/dalibo/transqlate/mysql"
)

func TestEngine(t *testing.T) {
	r := require.New(t)
	e := mysql.Engine()
	tree, err := e.Translate("<test>", "SELECT 1")
	r.Nil(err)
	r.Equal("SELECT 1", lexer.Write(tree))
}

func TestIfNull(t *testing.T) {
	r := require.New(t)
	sql, err := mysql.Translate("<test>", "SELECT IFNULL(1, 2)")
	r.Nil(err)
	r.Equal("SELECT COALESCE(1, 2)", sql)
}
