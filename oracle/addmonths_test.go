package oracle_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func TestAddMonth(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT add_months(sysdate, 1);")
	r.NoError(err)
	r.Equal("SELECT localtimestamp + interval '1 month';", sql)
}

func TestAddMonthNegative(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT add_months(date '2000-01-01', -1);")
	r.NoError(err)
	r.Equal("SELECT date '2000-01-01' - interval '1 month';", sql)
}

func TestAddTwoMonths(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT add_months(date '2000-01-01', 2);")
	r.NoError(err)
	r.Equal("SELECT date '2000-01-01' + interval '2 months';", sql)
}

func TestAddMonthsNonIntegerArgument(t *testing.T) {
	r := require.New(t)
	sql, _ := oracle.Translate("<stdin>", "SELECT add_months(sysdate, foo);")
	r.Equal("SELECT localtimestamp + foo * interval '1 month';", sql)
}

func TestAddMonthIndentation(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", `SELECT
	add_months(
		date '2000-01-01',
		1
	);`)
	r.NoError(err)
	r.Equal("SELECT\n\tdate '2000-01-01' + interval '1 month';", sql)
}
