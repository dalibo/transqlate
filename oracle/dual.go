package oracle

import (
	"gitlab.com/dalibo/transqlate/ast"
)

type removeFromDual struct{}

func (removeFromDual) String() string {
	return "remove FROM DUAL"
}

func (removeFromDual) Match(n ast.Node) bool {
	from, _ := n.(ast.From)
	if len(from.Tables) != 1 {
		return false
	}
	return from.Names()[0] == "DUAL"
}

func (removeFromDual) Rewrite(node ast.Node) (ast.Node, error) {
	return nil, nil
}
