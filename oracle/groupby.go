package oracle

import (
	"gitlab.com/dalibo/transqlate/ast"
)

type moveHavingAfterGroupBy struct{}

func (moveHavingAfterGroupBy) String() string {
	return "move HAVING after GROUP BY"
}

func (moveHavingAfterGroupBy) Match(n ast.Node) bool {
	having, _ := n.(ast.Having)
	return having.GroupBy != nil
}

func (moveHavingAfterGroupBy) Rewrite(n ast.Node) (ast.Node, error) {
	having := n.(ast.Having)
	groupby := having.GroupBy.(ast.GroupBy)
	having.GroupBy = nil
	groupby, having = ast.SwapWhitespaces(groupby, having)
	groupby.Having = having
	return groupby, nil
}
