package oracle_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func TestChangeCase(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", `SELECT UPPER, Mixed, lower, "QUPPER", "QMixed", "qlower"`)
	r.Nil(err)
	r.Equal(`SELECT UPPER, Mixed, lower, QUPPER, "QMixed", qlower`, sql)
}

func TestPreserveCase(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", `SELECT UPPER, Mixed, lower, "QUPPER", "QMixed", "qlower"`, oracle.PreserveCase(true))
	r.Nil(err)
	r.Equal(`SELECT "UPPER", "MIXED", "LOWER", "QUPPER", "QMixed", "qlower"`, sql)
}

func TestPreserveCaseBuiltins(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", `SELECT ABS(1), CUSTOM(), COALESCE(s)`, oracle.PreserveCase(true))
	r.Nil(err)
	// builtin is lowered, user defined is quoted, keyword is unchanged.
	r.Equal(`SELECT "abs"(1), "CUSTOM"(), COALESCE("S")`, sql)
}

func TestQuoteIdentifier(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", `SELECT "SELECT"`)
	r.Nil(err)
	r.Equal(`SELECT "SELECT"`, sql)
}
