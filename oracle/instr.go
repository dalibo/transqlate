package oracle

import (
	"fmt"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

type replaceInstr struct{}

func (replaceInstr) String() string {
	return "replace instr with position or regexp_instr"
}

func (replaceInstr) Match(n ast.Node) bool {
	c, _ := n.(ast.Call)
	return c.Is("INSTR")
}

func (replaceInstr) Rewrite(n ast.Node) (ast.Node, error) {
	c := n.(ast.Call)
	if len(c.Args) < 2 || 4 < len(c.Args) {
		return n, fmt.Errorf("INSTR with unhandled number of parameters")
	}

	position := 1
	if len(c.Args) > 2 {
		arg, _ := c.Args[2].Expression.(ast.Leaf)
		if arg.Token.Type != lexer.Integer {
			return n, fmt.Errorf("non literal position")
		}
		position = arg.Token.Int
	}

	f := c.Function.(ast.Leaf)
	if position > 1 || len(c.Args) == 4 {
		// INSTR(...)-> REGEXP_INSTR(...)
		f.Token.Set("regexp_instr")
		c.Function = f
		return c, nil
	}

	// INSTR('string', 'substring') -> POSITION('substring' IN 'string')
	f.Token.Set("position")
	c.Function = f
	// wraps 1st argument, the string, as IN clause
	in := ast.Clause{
		Keywords:   lexer.MustLex(" IN "),
		Expression: c.Args[0].Expression,
	}

	c.Clauses = append(c.Clauses, in)
	arg0 := c.Args[0]
	// Space substring (2nd argument) as first argument
	c.Args[1] = ast.SpaceFrom(c.Args[1], arg0)
	// keep only the 2nd argument, the substring
	c.Args = c.Args[1:2]
	// delete comma
	c.Args[0].Token = lexer.Token{}

	return c, nil
}
