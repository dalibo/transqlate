package oracle_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func TestInstr(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", `INSTR('foobarbar', 'bar')`)
	r.Nil(err)
	r.Equal(`position('bar' IN 'foobarbar')`, sql)

	sql, err = oracle.Translate("<test>", `INSTR('foobarbar', 'bar', 1)`)
	r.Nil(err)
	r.Equal(`position('bar' IN 'foobarbar')`, sql)

	sql, err = oracle.Translate("<test>", `INSTR('foobarbar', 'bar', 4)`)
	r.Nil(err)
	r.Equal(`regexp_instr('foobarbar', 'bar', 4)`, sql)

	sql, err = oracle.Translate("<test>", `INSTR('foobarbar', 'bar', 3, 2)`)
	r.Nil(err)
	r.Equal(`regexp_instr('foobarbar', 'bar', 3, 2)`, sql)
}
