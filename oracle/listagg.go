package oracle

import (
	"fmt"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

type listaggWithinGroup struct{}

func (listaggWithinGroup) String() string {
	return "rewrite LISTAGG() WITHIN GROUP"
}

func (listaggWithinGroup) Match(n ast.Node) bool {
	agg, _ := n.(ast.Aggregate)
	// Call node has already been renamed to STRING_AGG
	return agg.Call.Is("STRING_AGG") && agg.WithinGroup != nil
}

func (listaggWithinGroup) Rewrite(node ast.Node) (ast.Node, error) {
	agg := node.(ast.Aggregate)
	wg := agg.WithinGroup.(ast.Clause)
	clauses := wg.Expression.(ast.Array)
	if len(clauses.Items) != 1 {
		return nil, fmt.Errorf("multiple clauses in WITHIN GROUP")
	}
	orderBy, ok := clauses.Items[0].Expression.(ast.OrderBy)
	if !ok {
		return nil, fmt.Errorf("expected ORDER BY in WITHIN GROUP")
	}
	orderBy = orderBy.TransformStart(func(t lexer.Token) lexer.Token {
		return t.SpaceFrom(wg.Keywords[0])
	}).(ast.OrderBy)
	agg.Call.Clauses = append(agg.Call.Clauses, orderBy)
	agg.WithinGroup = nil
	agg = ast.SpaceFrom(agg, node)
	// Aggregate.Visit may squash Aggregate if Over is nil too.
	return agg, nil
}

type listaggOverflow struct{}

func (listaggOverflow) String() string {
	return "drop ON OVERFLOW"
}

func (listaggOverflow) Match(n ast.Node) bool {
	listagg, _ := n.(ast.Call)
	if !listagg.Is("LISTAGG") {
		return false
	}
	for _, clause := range listagg.Clauses {
		e, _ := clause.(ast.Expression)
		if e.Is("ON", "OVERFLOW") {
			return true
		}
	}
	return false
}

func (listaggOverflow) Rewrite(node ast.Node) (ast.Node, error) {
	listagg := node.(ast.Call)
	clauses := listagg.Clauses
	listagg.Clauses = nil
	for _, clause := range clauses {
		e, _ := clause.(ast.Expression)
		if !e.Is("ON", "OVERFLOW") {
			listagg.Clauses = append(listagg.Clauses, clause)
			break
		}
	}
	return listagg, nil
}
