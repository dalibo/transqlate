package oracle_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
	"gitlab.com/dalibo/transqlate/rewrite"
)

func TestWithinGroup(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", dedent.Dedent(`
	SELECT LISTAGG(employee) WITHIN GROUP (ORDER BY salary DESC)
	`)[1:], oracle.Defaults(), rewrite.Pretty())
	r.Nil(err)
	r.Equal(dedent.Dedent(`
	SELECT string_agg(employee, '' ORDER BY salary DESC)
	`)[1:], sql)
}

func TestListaggOverflow(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", dedent.Dedent(`
	SELECT LISTAGG(employee, ',' ON OVERFLOW TRUNCATE '...')
	`)[1:], oracle.Defaults(), rewrite.Pretty())
	r.Nil(err)
	r.Equal(dedent.Dedent(`
	SELECT string_agg(employee, ',')
	`)[1:], sql)
}
