package oracle

import (
	"errors"

	"gitlab.com/dalibo/transqlate/ast"
)

type isnull struct {
}

func (isnull) String() string {
	return "IS NULL operation"
}

func (isnull) Match(v ast.Node) bool {
	is, _ := v.(ast.Infix)
	if !is.Is("IS") {
		return false
	}

	null, _ := is.Right.(ast.Leaf)
	if null.In("NULL") {
		// Found IS NULL
		return true
	}
	not, _ := is.Right.(ast.Prefix)
	if !not.Is("NOT") {
		// Cannot be NOT NULL
		return false
	}
	null, _ = not.Expression.(ast.Leaf)
	return null.In("NULL")
}

func (isnull) Rewrite(n ast.Node) (ast.Node, error) {
	return n, errors.New("NULL comparison on identifier")
}
