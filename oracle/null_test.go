package oracle_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func TestIsNull(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", `a IS NULL`)
	r.Error(err)
	r.Equal(`a IS NULL`, sql)
}

func TestIsNotNull(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", `a IS NOT NULL`)
	r.Error(err)
	r.Equal(`a IS NOT NULL`, sql)
}

func TestIsEmptyString(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<test>", `a IS ''`)
	r.NoError(err)
	r.Equal(`a IS ''`, sql)
}
