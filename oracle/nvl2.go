package oracle

import (
	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/internal/parser"
	"gitlab.com/dalibo/transqlate/lexer"
)

type rewriteNvl2AsCase struct{}

func (rewriteNvl2AsCase) String() string {
	return "rewrite NVL2() as CASE"
}

func (rewriteNvl2AsCase) Match(n ast.Node) bool {
	c, _ := n.(ast.Call)
	if len(c.Args) != 3 {
		return false
	}
	return c.Is("NVL2")
}

func (rewriteNvl2AsCase) Rewrite(n ast.Node) (ast.Node, error) {
	items := n.(ast.Call).Args

	c := parser.MustParse("CASE WHEN 0 IS NOT NULL THEN 0 ELSE 0 END").(ast.Case)

	// Replace the WHEN expression.
	c.Cases[0].When.Expression = ast.Infix{
		Left:  items[0].Expression,
		Op:    lexer.MustLex(" IS"),
		Right: parser.MustParse(" NOT NULL"),
	}

	// Replace the THEN expression.
	c.Cases[0].Then.Expression = items[1].Expression

	// Replace the ELSE expression.
	c.Else.Expression = items[2].Expression

	return spaceCaseFromNvl2(c, n.(ast.Call)), nil
}

// spaceCaseFromNvl2 sets indentation for CASE expression.
//
// Preserves indentation of NVL2 function and its arguments.
func spaceCaseFromNvl2(c ast.Case, nvl ast.Call) ast.Node {
	c = ast.SpaceFrom(c, nvl)
	c.Case.Suffix = nvl.Open.Suffix

	w := c.Cases[0]

	w.When = ast.SpaceFromItem(w.When, nvl.Args, 0)
	w.When.Expression = ast.OneSpace(w.When.Expression)
	if !nvl.Open.EndsLine() {
		w.When.Token.Prefix = " "
	}

	w.Then = ast.SpaceFromItem(w.Then, nvl.Args, 1)
	w.Then.Expression = ast.OneSpace(w.Then.Expression)

	c.Cases[0] = w

	c.Else = ast.SpaceFromItem(c.Else, nvl.Args, 2)
	c.Else.Expression = ast.OneSpace(c.Else.Expression)

	c.End = c.End.SpaceFrom(nvl.Close)
	if !nvl.Close.BeginsLine() {
		c.End.Prefix = " "
	}

	return c
}
