package oracle_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func TestNvl2(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", `nvl2(a, b, "default")`)
	r.NoError(err)
	r.Equal(`CASE WHEN a IS NOT NULL THEN b ELSE "default" END`, sql)
}

func TestNvl2Indented(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", dedent.Dedent(`
	NVL2(
		a,
		b,
		"default"
	)
	`))

	r.NoError(err)
	r.Equal(dedent.Dedent(`
	CASE
		WHEN a IS NOT NULL
		THEN b
		ELSE "default"
	END
	`), sql)
}

func TestNvl2Comment(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", dedent.Dedent(`
	nvl2(
		-- comment
		a,
		b, -- comment
		"default" -- comment
	)
	`))
	r.NoError(err)
	r.Equal(dedent.Dedent(`
	CASE
		-- comment
		WHEN a IS NOT NULL
		THEN b -- comment
		ELSE "default" -- comment
	END
	`), sql)
}
