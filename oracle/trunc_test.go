package oracle_test

import (
	"testing"

	"github.com/lithammer/dedent"
	"github.com/stretchr/testify/require"
	"gitlab.com/dalibo/transqlate/oracle"
)

func TestTruncWithoutFormat(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT TRUNC(SYSDATE);")
	r.NoError(err)
	r.Equal("SELECT DATE_TRUNC('day', LOCALTIMESTAMP);", sql)
}

func TestTruncWithFormat(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", "SELECT trunc(sysdate, 'DD');")
	r.NoError(err)
	r.Equal("SELECT date_trunc('day', localtimestamp);", sql)
}

func TestTruncWithComputedFormat(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", `SELECT "TRUNC"(sysdate, compute());`)
	r.Error(err)
	// Ensure the SQL is still partially translated.
	r.Equal(`SELECT DATE_TRUNC(compute(), localtimestamp);`, sql)
}

func TestTruncSpacings(t *testing.T) {
	r := require.New(t)
	sql, err := oracle.Translate("<stdin>", dedent.Dedent(`
	SELECT
		TRUNC(
			SYSDATE,
			-- fmt comment
			'dd'
		)
	FROM
		DUAL
	`))

	r.NoError(err)
	r.Equal(dedent.Dedent(`
	SELECT
		DATE_TRUNC(
			-- fmt comment
			'day',
			LOCALTIMESTAMP
		)
	`), sql)
}
