package rewrite

import (
	"errors"
	"fmt"
	"log/slog"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// Engine holds rules to rewrite an AST.
type Engine struct {
	name       string
	parse      parseFunc
	nodeRules  []nodeRule
	treeRules  []treeRule
	tokenRules []tokenRule
}

type parseFunc func(source string, input string) (ast.Node, error)

// nodeRule is a rule that matches and rewrites a node in a tree.
type nodeRule interface {
	fmt.Stringer
	Match(ast.Node) bool
	Rewrite(ast.Node) (ast.Node, error)
}

// tokenRule filters token to modify case, quoting, etc.
type tokenRule interface {
	fmt.Stringer
	Rewrite(lexer.Token) lexer.Token
}

// treeRule is called once with root node only.
//
// Use it to reindent a tree.
type treeRule interface {
	fmt.Stringer
	Rewrite(ast.Node) ast.Node
}

// Option is a function that applies options to an Engine.
type Option func(*Engine)

// New creates a new Engine with the given name and options.
func New(name string, o ...Option) Engine {
	e := Engine{
		name: name,
	}
	e.Apply(o...)
	return e
}

// String returns the name of the engine.
func (e Engine) String() string {
	return e.name
}

// Apply options to the engine.
func (e *Engine) Apply(o ...Option) {
	for _, f := range o {
		if f == nil {
			continue
		}
		f(e)
	}
}

// Append rules to the engine.
//
// Rules can be any of ast.NodeRule, TreeRule, TokenRule.
func (e *Engine) Append(rules ...interface{}) {
	for _, rule := range rules {
		switch r := rule.(type) {
		case nodeRule:
			e.nodeRules = append(e.nodeRules, r)
		case treeRule:
			e.treeRules = append(e.treeRules, r)
		case tokenRule:
			e.tokenRules = append(e.tokenRules, r)
		default:
			panic("unknown rule type")
		}
	}
}

// Translate source input SQL into PostgreSQL dialect.
//
// Returns the flattened tokens and a single error that wraps all errors.
func (e Engine) Translate(source, input string) (lexer.Tokener, error) {
	tree, err := e.parse(source, input)
	if err != nil {
		return nil, fmt.Errorf("parse: %w", err)
	}
	return e.Run(tree)
}

// Parse input SQL into an AST.
func (e Engine) Parse(source, input string) (ast.Node, error) {
	return e.parse(source, input)
}

// Run rules on tree.
//
// Returns the flattened tokens and a single error that wraps all errors.
//
// Execute rules in the following order:
//
//   - node rules on each nodes, depth first.
//   - tree rules on root node only.
//   - token rules on each flattened tokens.
func (e Engine) Run(n ast.Node) (out lexer.Tokener, err error) {
	n, err = e.nodes(n)
	n = e.tree(n)
	out = e.tokens(n)
	return
}

func (e Engine) nodes(n ast.Node) (ast.Node, error) {
	var allErrs []error
	n = n.Visit(func(n ast.Node) ast.Node {
		var err error
		var errs []error
		for _, r := range e.nodeRules {
			if !r.Match(n) {
				continue
			}
			slog.Debug("Applying node rule.", "rule", r, "node", lexer.ShortWrite(n))
			perr := wrapPanic(func() {
				n, err = r.Rewrite(n)
			})
			if perr != nil {
				err = perr
			}
			if err != nil {
				errs = append(errs, Err{Err: err, Rule: r})
			}
			if n == nil {
				slog.Debug("ast.Node removed.", "rule", r)
				return nil // Ignore errors
			}
		}

		// Attach final node to each errors.
		for i := range errs {
			e := errs[i].(Err)
			e.Node = n
			errs[i] = e
		}

		// Attach all errors to first token of ast.Node for highlighting.
		n = n.TransformStart(func(t lexer.Token) lexer.Token {
			t.Error = errors.Join(errs...)
			return t
		})

		// Collect all errors.
		allErrs = append(allErrs, errs...)

		return n
	})

	return n, errors.Join(allErrs...)
}

func (e Engine) tree(n ast.Node) ast.Node {
	for _, r := range e.treeRules {
		slog.Debug("Applying tree rule.", "rule", r)
		n = r.Rewrite(n)
	}
	return n
}

func (e Engine) tokens(input lexer.Tokener) lexer.Tokener {
	if input == nil {
		return input
	}

	if len(e.tokenRules) == 0 {
		return input
	}

	var output lexer.Tokens
	for _, r := range e.tokenRules {
		output = nil
		slog.Debug("Applying token rule.", "rule", r)
		for _, t := range input.Tokens() {
			output = append(output, r.Rewrite(t))
		}
		input = output // Loop for next rules.
	}

	return output
}

// Parser sets parse func of engine
//
// Not setting parseFunc panics on Translate and Parse.
func Parser(f parseFunc) Option {
	return func(e *Engine) {
		e.parse = f
	}
}
