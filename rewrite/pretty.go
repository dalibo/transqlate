package rewrite

import (
	"strings"

	"gitlab.com/dalibo/transqlate/ast"
	"gitlab.com/dalibo/transqlate/lexer"
)

// Pretty configures engine to prettify output.
//
// It appends the following rules:
//
//   - Uppercase keywords
//   - Reindent
//   - Lowercase identifiers
//   - Unquote unambiguous identifiers
//
// transqlate implements Simon HOLYWELL's SQL style.
// See https://www.sqlstyle.guide/ .
func Pretty() Option {
	return func(e *Engine) {
		e.Append(
			upperKeywordsRule{},
			lowerIdentifiers{},
			indentRule{},
		)
	}
}

type upperKeywordsRule struct{}

var _ tokenRule = upperKeywordsRule{}

func (upperKeywordsRule) String() string {
	return "upper case keywords"
}

func (upperKeywordsRule) Rewrite(token lexer.Token) lexer.Token {
	if token.Type != lexer.Keyword {
		return token
	}
	token.Set(strings.ToUpper(token.Str))
	return token
}

type lowerIdentifiers struct{}

var _ tokenRule = lowerIdentifiers{}

func (lowerIdentifiers) String() string {
	return "lower case identifiers"
}

func (lowerIdentifiers) Rewrite(token lexer.Token) lexer.Token {
	if token.Type != lexer.Identifier {
		return token
	}

	if token.Quoted() {
		isLower := strings.ToLower(token.Raw) == token.Raw
		if !isLower {
			return token
		}
		if lexer.IsKeyword(token.Str) {
			return token
		}
		// Unquote unambiguous identifier.
		token.Set(token.Str)
	}
	token.Set(strings.ToLower(token.Str))
	return token
}

type indentRule struct{}

var _ treeRule = indentRule{}

func (indentRule) String() string {
	return "indent"
}

func (indentRule) Rewrite(tree ast.Node) ast.Node {
	return ast.Indent(tree, "")
}
