SELECT r.last_name,
       -- comment
       (SELECT max(year(championship_date))
          -- comment
          FROM champions AS c
         WHERE c.last_name = r.last_name
           AND c.confirmed = 'Y') AS last_championship_year
  FROM riders AS r
 -- comment
 WHERE r.last_name IN
       -- comment
       (SELECT c.last_name
          FROM champions AS c
         WHERE year(championship_date) > '2008'
           -- comment
           AND c.confirmed = 'Y');
