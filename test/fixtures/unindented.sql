select r.last_name,
-- comment
(select max(year(championship_date))
-- comment
from champions as c
where c.last_name = r.last_name
and c.confirmed = 'Y') as last_championship_year
from riders as r
-- comment
where r.last_name in
-- comment
(select c.last_name
from champions as c
where year(championship_date) > '2008'
-- comment
and c.confirmed = 'Y');
