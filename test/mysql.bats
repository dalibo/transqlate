#!/usr/bin/env bats

bats_require_minimum_version 1.5.0

@test "mysql" {
	run --separate-stderr transqlate --dialect=mysql <<<'IFNULL(1, 2)'
	[ "$status" -eq 0 ]
	[ "${lines[0]}" = 'COALESCE(1, 2)' ]
}
